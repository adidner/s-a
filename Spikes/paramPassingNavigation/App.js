
{/*react imports*/}
import React from 'react';
import { Button, View, Text } from 'react-native';
import { createStackNavigator } from 'react-navigation';


{/*classes imports*/}
import Role from './classes/classesRoles';
import player from './classes/classesPlayers';


{/*componentView imports*/}
import DetailsScreen from './componentViews/roleChoosing';
import howToPlay from './componentViews/howToPlay';
import contactSearch from './componentViews/contactSearch';
import alternatecontactSearch from './componentViews/alternatecontactSearch';



class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Main Menu',
  };
  
   
  render() {
	

	{/*important that these go before the return but inside the render*/}	
	witness = new Role("Witness", "Hides and if he gets a lot of good guys with him wins, a lot of bad guys looses",0);
	doctor = new Role("Doctor", "bring the dead back to life by drawing a cross on their arm",0);
	murderer = new Role("murderer","kill people by running your hand across their neck",0);
	
	var rolePool = [witness, doctor, murderer];
	
	
	
    return (
	
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
		
		
		{/*important to have the braces around the class use*/}
		<Text>{rolePool[1].details}</Text>
		
		
		
		<Text>Witness</Text>

		<Button
          title="Go to Details"
          onPress={() => this.props.navigation.navigate('Details',{rolePoolParam:rolePool})}
        />
		<Button
          title="Go to contactSearch"
          onPress={() => this.props.navigation.navigate('contactSearch',{rolePoolParam:rolePool})}
        />
		<Button
          title="Go to howToPlay"
          onPress={() => this.props.navigation.navigate('howToPlay')}
        />
		<Button
          title="Go to alternatecontactSearch"
          onPress={() => this.props.navigation.navigate('alternatecontactSearch')}
        />
      </View>
    );
  }
}



const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    Details: DetailsScreen,
	howToPlay: howToPlay,
	contactSearch: contactSearch,
	alternatecontactSearch: alternatecontactSearch
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}