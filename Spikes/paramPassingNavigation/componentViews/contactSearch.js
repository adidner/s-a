import React from 'react';
import { Button, View, Text, FlatList, TextInput, StyleSheet, Alert } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Contacts from 'react-native-contacts';


export default class contactSearch extends React.Component {
  
  static navigationOptions = {
    title: 'Role Choosing',
  };
  
  render() {
	  
	const { navigation } = this.props;
    const rolePool = navigation.getParam('rolePoolParam', 'NO-ID');
    
	

    return (
	<View style={{padding: 10}}>
		<TextInput
          style={{height: 40}}
          placeholder="Enter a contacts name"
          onChangeText={(text) => this.setState({text})}
        />
		<FlatList
          data={[
            {key: 'Devin'},
            {key: 'Jackson'},
            {key: 'James'},
            {key: 'Joel'},
            {key: 'John'},
            {key: 'Jillian'},
            {key: 'Jimmy'},
            {key: 'Julie'},
			{key: 'Devin'},
            {key: 'Jackson'},
            {key: 'James'},
            {key: 'Joel'},
            {key: 'John'},
            {key: 'Jillian'},
            {key: 'Jimmy'},
            {key: 'Julie'},
			{key: 'Devin'},
            {key: 'Jackson'},
            {key: 'James'},
            {key: 'Joel'},
            {key: 'John'},
            {key: 'Jillian'},
            {key: 'Jimmy'},
            {key: 'Julie'},
          ]}
          renderItem={({item}) => <Button
			  onPress={() => {
				Alert.alert('You tapped the button! ' + item.key);
			  }}
			  title={item.key}
			/>}
        />
     </View>
	  
    );
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 22
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
})