import React from 'react';
import { Button, View, Text } from 'react-native';
import { createStackNavigator } from 'react-navigation';

export default class DetailsScreen extends React.Component {
  
  static navigationOptions = {
    title: 'Role Choosing',
  };
  
  render() {
	  
	const { navigation } = this.props;
    const rolePool = navigation.getParam('rolePoolParam', 'NO-ID');
    	  
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Details Screen</Text>
		<Text>{rolePool[1].name}</Text>
		<Text>{rolePool[2].details}</Text>
		<Text>{rolePool[0].roleNumber}</Text>
      </View>
    );
  }
}
