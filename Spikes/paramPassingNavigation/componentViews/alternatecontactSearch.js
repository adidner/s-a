import React, { Component } from 'react';
import { View, Text, FlatList, TextInput, StyleSheet, Alert } from 'react-native';
import { Button } from 'react-native-elements'; // Version can be specified in package.json
import { Permissions, Contacts } from 'expo';

import SearchContacts from '../classes/classesSearchContacts';

export default class alternatecontactSearch extends Component {
  
  
	
  
	constructor(props) {
		super(props);
		this.state = {contacts: [{key:'default nothign'}], contactlist: ""};
		
	}
	
	
	
  
  async showFirstContactAsync(changedtext) {
      
	  
	  console.log("in aysync");
	  
	  // Ask for permission to query contacts.
      const permission = await Permissions.askAsync(Permissions.CONTACTS);
      
      if (permission.status !== 'granted') {
        // Permission was denied...
		console.log("denied");
        return;
      }
	  {/*800 to get your first 800 contacts*/}
      const contacts = await Contacts.getContactsAsync({
        fields: [
          Contacts.PHONE_NUMBERS,
          Contacts.EMAILS,
        ],
		pageSize: 800,
        pageOffset: 0,
      });
	  
	
	  
      if (contacts.total > 0) {
		newcontactlist = [];
		for(var i =0; i < contacts.data.length; i++){
			
			if(contacts.data[i].name.includes(changedtext)){
				newcontactlist.push({key: contacts.data[i].name, phoneNumber: contacts.data[i].phoneNumbers[0].number});
			}
		}
		
		this.setState({contactlist: newcontactlist});
		 
      }
	  else{
		Alert.alert(
          'You have no contacts'
        );  
	  }
	  
    }
	
	doesthing(){
		console.log('in does things functino');
	}
	

    
  render() {
    return (
	
      
	  
	  <View style={{padding: 10}} 
	  keyboardShouldPersistTaps="always">
		<TextInput
          style={{height: 40}}
          placeholder="Enter a contacts name"
          onChangeText={(changedtext) => this.showFirstContactAsync(changedtext)}
        />
		<FlatList
		  keyboardShouldPersistTaps="always"
          data={this.state.contactlist}
		  extraData={this.state}
          renderItem={({item}) => <Button
				  onPress={() => {
					Alert.alert(
					  'You have no contacts' + 
					  item.key +
					  item.phoneNumber
					); 
				  }}
				  title={item.key}
				/>}
			/>
		</View>
	
    );
  }
}