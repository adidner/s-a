import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

{/*classes imports*/}
import Role from './classes/classesRoles';
import player from './classes/classesPlayers';

const BASE_URL = 'https://nowserver-wifamyvjtm.now.sh';

export default class App extends React.Component {
  
	witness = new Role("Witness", "Hides and if he gets a lot of good guys with him wins, a lot of bad guys looses",0);
	doctor = new Role("Doctor", "bring the dead back to life by drawing a cross on their arm",0);
	murderer = new Role("murderer","kill people by running your hand across their neck",0);
	
  
  
  
  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
		
		<Button
		  onPress={this.onPressCommaLog}
		  title="comma deliniate the list and log it"
		/>
		
		<Button
		  onPress={this.onPressExpoSide}
		  title="expo Side error test"
		/>
		
      </View>
    );
  }
  
  onPressCommaLog(){
	  var array = ['name','description','name','description','name','description','name','descriptiono'];
	  var output = array.join(',');
	  console.log(output);
	  var array =  output.split(",");
	  console.log(array);
  }
  
  
 
  
  
  
  onPressExpoSide = async () => {
	  
		console.log('Begining of function');
		
	    var name = ["doctor","witness","murderer"];
		var descriptions = ["revives peopele","hides","kills"];
		var numbers = ["5039363436","5039363436","5039363436"];
		nametogether = name.join(',');
		descriptionstogether = descriptions.join(',');
		numberstogether = numbers.join(',');
		
		try {
		console.log('In Try');
		fetch(`${BASE_URL}/`, {
		  method: 'POST',
		  headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
		  },
		  body: JSON.stringify({
			namepass: nametogether,
			namestuff: 'yourOtherValue',
		  }),
		});
		console.log('In Try');
		}catch (e) {
		  alert('Error, sorry! :(');
		}
		
		console.log('End of function');
		
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
