import React from 'react';

export default class Role {
  
  
  constructor(name,details,roleNumber) {
    this.name = name;
    this.details = details;
	this.roleNumber = roleNumber;
  }
}