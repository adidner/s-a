import React from 'react';
import { Button, View, Text } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import DetailsScreen from './classes/roleChoosing';
import howToPlay from './classes/howToPlay';

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Main Menu',
  };
  
  
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
		<Button
          title="Go to Details"
          onPress={() => this.props.navigation.navigate('Details')}
        />
		<Button
          title="Go to howToPlay"
          onPress={() => this.props.navigation.navigate('howToPlay')}
        />
      </View>
    );
  }
}



const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    Details: DetailsScreen,
	howToPlay: howToPlay,
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}