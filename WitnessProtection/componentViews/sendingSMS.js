import React from 'react';
import { AsyncStorage, View, Text, StyleSheet, FlatList, TextInput, TouchableOpacity} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Modal from 'react-native-modal'; // 2.4.0
import {Button, Icon, CheckBox} from 'react-native-elements'

import player from '../classes/classesPlayers';

import styles from '../styles';

const BASE_URL = 'https://nowserver-higfhftilz.now.sh';


export default class sendingSMS extends React.Component {

  constructor(props) {
		super(props);
		const { navigation } = this.props;
		const rolePoolT = navigation.getParam('rolePoolParam', 'NO-ID');
		const totalRolesT = navigation.getParam('totalRolesParam', 'NO-ID');


		playerPoolT = [];
		for(i = 0;i<totalRolesT;i++){
			tempPlayer = new player("","");
			playerPoolT.push(tempPlayer);
		}


		this.state = {rolePool: rolePoolT,
			totalRoles: totalRolesT,
			playerPool: playerPoolT,
			searchResult: null,
			quickAddModalVisible: false,
			quickRemoveNameModalVisible: false,
			quickRemoveRoleModalVisible: false,
			quickRemoveModifiedRoleList: [],
			saveModal: false,
			saveModal2: false,
			loadModal: false,
			savetitle: '',
			online: true,
			offline: false,
			title1: 'in1',
			title2: 'in2',
			title3: 'in3',
			keyboardtype: 'default',
			guideModal: false,
			checkedSendText: false,
		}

	}

  toggleModal() {
      newState = this.state;
	  console.log('initial state: '+ newState.quickAddModalVisible);
	  newState.quickAddModalVisible = !(this.state.quickAddModalVisible);
	  console.log('modified state: '+ newState.quickAddModalVisible);
	  this.setState(newState);
   }

   toggleRemoveNameModal(){
	   newState = this.state;
	   newState.quickRemoveNameModalVisible = !(this.state.quickRemoveNameModalVisible);
	   this.setState(newState);
   }

   toggleRemoveRoleModal(){
	   newState = this.state;
	   newState.quickRemoveRoleModalVisible = !(this.state.quickRemoveRoleModalVisible);
	   this.setState(newState);
   }


   togglesaveModal(){
	   newState = this.state;
	   newState.saveModal = !(this.state.saveModal);
	   this.setState(newState);
	   console.log('in save modal toggle');
   }
   togglesaveModal2(){
	   newState = this.state;
	   newState.saveModal2 = !(this.state.saveModal2);
	   this.setState(newState);
   }

   toggleloadModal(){
	   newState = this.state;
	   newState.loadModal = !(this.state.loadModal);
	   this.setState(newState);

   }





  refreshFromAlternateContactSearch(name,number){
	number = number.replace(/\D/g,'');

	tempState = this.state;
	for(i = 0; i< tempState.totalRoles;i++){
		if(tempState.playerPool[i].name=='' && tempState.playerPool[i].phoneNumber==''){
			console.log('Empty name, index: ' + i);
			tempState.playerPool[i].name = name;
			tempState.playerPool[i].phoneNumber = number;
			this.setState(tempState);
			i=tempState.totalRoles;
		}
	}

  }

  static navigationOptions = {
    title: 'Send Texts',
  };

  controlName(value, index){
	tempState = this.state;
	tempState.playerPool[index].name = value;
	console.log(tempState);
	this.setState(tempState);
  }


   controlNumber(value, index){
	tempState = this.state;
	tempState.playerPool[index].phoneNumber = value;
	console.log(tempState);
	this.setState(tempState);
  }

  _quickAdd(index){
	console.log("in method");
	console.log("initial: "+JSON.stringify(this.state));
	newstate = this.state;
	newstate.rolePool[index].roleNumber = newstate.rolePool[index].roleNumber+1;
	newstate.quickAddModalVisible = false;
	newstate.totalRoles++;
	tempPlayer = new player("","");
	newstate.playerPool.push(tempPlayer);
	this.setState(newstate);
	console.log("modified state: "+JSON.stringify(this.state));

  }

  _quickRemoveName(index){
	  console.log('in method, quickRemoveName');
	  console.log(JSON.stringify(this.state.playerPool));
	  newstate = this.state;
	  newstate.playerPool.splice(index,1);

	  for( i = 0; i < newstate.rolePool.length; i++){
		  if(newstate.rolePool[i].roleNumber > 0){
			  newstate.quickRemoveModifiedRoleList.push(newstate.rolePool[i]);
		  }
	  }

	  this.setState(newstate);
	  this.toggleRemoveNameModal();
	  this.toggleRemoveRoleModal();
	  console.log(JSON.stringify(this.state.playerPool));
  }

  _quickRemoveRole(name){
	  console.log('in method, quickRemoveRole');
	  console.log(JSON.stringify(this.state));
	  newstate=this.state;
	  for(i = 0; i<newstate.rolePool.length;i++){
		if(name==newstate.rolePool[i].name){

			newstate.rolePool[i].roleNumber--;
		}
	  }
	  newstate.totalRoles--;
	  this.setState(newstate);
	  this.toggleRemoveRoleModal();
	  console.log(JSON.stringify(this.state));

  }


	_assignRoles(){

		console.log('in assign roles');
		oneToOneList = [];
		for(j = 0; j < this.state.rolePool.length; j++){
			for(i = 0; i<this.state.rolePool[j].roleNumber; i++){
				rolecopy = this.state.rolePool[j];
				oneToOneList.push(rolecopy);
			}
		}

		console.log('one to one lsit: '+JSON.stringify(oneToOneList));
		oneToOneList.sort(function(a, b){return 0.5 - Math.random()});
		newstate = this.state;
		for(i = 0; i< this.state.playerPool.length; i++){
			newstate.playerPool[i].playerRole = oneToOneList[i];
		}
		console.log('plyaerPOol: '+JSON.stringify(newstate.playerPool));
		this.setState(newstate);
	}

	_sendText(){
		this._assignRoles();
		this.onPressExpoSide();
		this.props.navigation.navigate('hideTimer', {online: this.state.online,offline:this.state.offline});
	}


	onPressExpoSide = async () => {

		console.log('Begining of function');

	    playerPoolList = this.state.playerPool;
		playerPoolList = JSON.stringify(playerPoolList);
		//playerPoolList = playerPoolList.join(',');
		console.log('String Player Pool List: ' + playerPoolList);

		try {
			console.log('In Try');
			fetch(`${BASE_URL}/`, {
			  method: 'POST',
			  headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			  },
			  body: JSON.stringify({
				stringPlayerPoolList: playerPoolList,

			  }),
			});

		}catch (e) {
		  alert('Error, sorry! :(');
		}

		console.log('End of function');

  }


  _storeData = async (slot) => {
	  console.log('in _storeData')
	  try {
		string = '@Save'+slot+':state';
		savestate = JSON.stringify(this.state);
		await AsyncStorage.setItem(string, savestate);
	  } catch (error) {
		// Error saving data
		console.log('error in asyncStorage')
	  }
	}

	_retrieveData = async (slot) => {
		console.log('in retrieveData');
	  try {
		string = '@Save'+slot+':state';
		const value = await AsyncStorage.getItem(string);
		console.log("Retrieved Value: "+  value);
		if (value != null) {
			console.log("title value: "+ value.savetitle);
			objectversion = JSON.parse(value);
			newplayerpool = [];
			smallersize = 0;
			console.log("playerPoollen: "+this.state.totalRoles);
			console.log("totalRoleslen: "+objectversion.playerPool.length);
			modifiedplayerpool=this.state.playerPool;
			if(objectversion.playerPool.length <= this.state.totalRoles){
				smallersize=objectversion.playerPool.length;
			}
			else if(objectversion.playerPool.length >= this.state.totalRoles){
				smallersize=this.state.totalRoles;
			}
			for(i = 0; i < smallersize;i++){
				console.log(objectversion.playerPool[i]);
				modifiedplayerpool[i]=objectversion.playerPool[i];
			}
			console.log("new player pool: " + newplayerpool);
			console.log("old player pool: "+ this.state.playerPool);
			this.setState({playerPool: modifiedplayerpool});
			return;
		}
		else{
			console.log('null section');
			return;
		}
	   } catch (error) {
		 // Error retrieving data
		 console.log('error in retrieveData');
	   }
	}

  savetitle(newtitle){
	 newstate = this.state;
	 newstate.savetitle=newtitle;
	 this.setState(newstate);
  }

	deletetitle1 = async () => {
	  try {
		this.setState({title1:'Empty'})
		string = '@Save1:state';
		await AsyncStorage.removeItem(string);
	  } catch (error) {
		// Error retrieving data
		console.log(error.message);
	  }
	}

	deletetitle2 = async () => {
	  try {
		this.setState({title2:'Empty'})
		string = '@Save2:state';
		await AsyncStorage.removeItem(string);
	  } catch (error) {
		// Error retrieving data
		console.log(error.message);
	  }
	}

	deletetitle3 = async () => {
	  try {
		this.setState({title3:'Empty'})
		string = '@Save3:state';
		await AsyncStorage.removeItem(string);
	  } catch (error) {
		// Error retrieving data
		console.log(error.message);
	  }
	}

  settitles = async () => {
	  console.log('in newgettitlessss');
	  try {
		string = '@Save1:state';
		const value = await AsyncStorage.getItem(string);
		if (value != null) {
			objectversion = JSON.parse(value);
			console.log("title object, save: " + string + ",  Title:" + objectversion.savetitle);
			this.setState({title1:objectversion.savetitle})
		}
		else{
			console.log('in the null section');
			this.setState({title1:'Empty'});
		}
	   } catch (error) {
		 // Error retrieving data
		 console.log('error in retrieveData');
	   }

	   try {
		string = '@Save2:state';
		const value = await AsyncStorage.getItem(string);
		if (value != null) {
			objectversion = JSON.parse(value);
			console.log("title object, save: " + string + ",  Title:" + objectversion.savetitle);
			this.setState({title2:objectversion.savetitle})
		}
		else{
			console.log('in the null section');
			this.setState({title2:'Empty'});
		}
	   } catch (error) {
		 // Error retrieving data
		 console.log('error in retrieveData');
	   }

	   try {
		string = '@Save3:state';
		const value = await AsyncStorage.getItem(string);
		if (value != null) {
			objectversion = JSON.parse(value);
			console.log("title object, save: " + string + ",  Title:" + objectversion.savetitle);
			this.setState({title3:objectversion.savetitle})
		}
		else{
			console.log('in the null section');
			this.setState({title3:'Empty'});
		}
	   } catch (error) {
		 // Error retrieving data
		 console.log('error in retrieveData');
	   }
	}


	is1(){
		if(this.state.title1=='Empty'){
			return '';
		}
		else{
			return styles.modalbuttonSjustred;
		}
	}
	is2(){
		if(this.state.title2=='Empty'){
			return '';
		}
		else{
			return styles.modalbuttonSjustred;
		}
	}
	is3(){
		if(this.state.title3=='Empty'){
			return '';
		}
		else{
			return styles.modalbuttonSjustred;
		}
	}

	_storeDataSendText = async () => {
	  try {

		string = '@checkedSendText:key';
		savedata='';
		if(!this.state.checkedSendText){
			savedata = 'true';
		}
		else{
			savedata='false';
		}
		await AsyncStorage.setItem(string, savedata);
	  } catch (error) {
		// Error saving data
		console.log('error in asyncStorage Roles')
	  }
	}

	_retrieveDataSendText = async () => {
		console.log('in retrieveData Roles');
	  try {
		string = '@checkedSendText:key';
		const value = await AsyncStorage.getItem(string);
		console.log("Retrieved Value: "+  value);
		if (value == null || value=='true') {
			newstate=this.state;
			newstate.guideModal=true;
			this.setState(newstate);
			return;
		}
		else{
			newstate=this.state;
			newstate.guideModal=false;
			this.setState(newstate);
			return;
		}
	   } catch (error) {
		 // Error retrieving data
		 console.log('error in retrieveData');
	   }
	}

	componentDidMount(){
	  this._retrieveDataSendText();
	}




  render() {

    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

		<View style={styles.headerbox} containerViewStyle={{ width: '100%', height: '10%', margin: '2%' }}>





			<View style= {styles.iconcontainerSMS}>
				<Icon
				 onPress={() => {
					this.settitles();this.toggleloadModal();
				  }}
				  name='md-download'
				  type='ionicon'
				  iconStyle={styles.roleheadericonSMS}
				/>
				<Text style={styles.iconbelowtextSMS}> Load </Text>
			</View>


			<View style= {styles.iconcontainerSMS}>
				<Icon
				  name='save'
				  type='foundation'
				  onPress={() => {
					 this.settitles();this.togglesaveModal();
				  }}
				  iconStyle={styles.roleheadericonSMS}
				/>
				<Text style={styles.iconbelowtextSMS}> Save </Text>
			</View>


			<View style= {styles.iconcontainerSMS}>
				<Icon
						 onPress={() => {
					this.toggleModal()
				  }}
				name='user-plus'
				type='feather'
				iconStyle={styles.roleheadericonSMS}
				/>
				<Text style={styles.iconbelowtextSMS}> Add </Text>
			</View>


			<View style= {styles.iconcontainerSMS}>
				<Icon
						 onPress={() => {
					this.toggleRemoveNameModal()
				  }}
				name='user-minus'
				type='feather'
				iconStyle={styles.roleheadericonSMS}
				/>
				<Text style={styles.iconbelowtextSMS}> Remove </Text>
			</View>


			<View style= {styles.iconcontainerSMS}>
				<Icon
					onPress={() =>
					this.props.navigation.navigate('alternatecontactSearch', {
					  refresh: this.refreshFromAlternateContactSearch.bind(this)
					})
				  }
				  name='search'
				  type='feather'
				  iconStyle={styles.roleheadericonSMS}
				/>
				<Text style={styles.iconbelowtextSMS}> Search </Text>
			</View>


			<View style= {styles.iconcontainerSMS}>
					<Icon
					 onPress={() => {
							this._sendText()
					  }}
					  name='ios-send'
					  type='ionicon'
					  iconStyle={styles.roleheadericonSMS}
					/>
					<Text style={styles.iconbelowtextSMS}> Send </Text>
			</View>


		</View>

		<FlatList
          data={this.state.playerPool}
		  extraData={this.state}
		  style={{flex:1, paddingTop: 22}}
		  keyboardShouldPersistTaps='always'

		  keyExtractor={(item,index) => index.toString()}
		  renderItem={({item, index}) =>(

			<View style={{flexDirection: 'row'}}>

				{/*<Text style={styles.item}>{item.name}</Text>*/}
				<TextInput
				placeholder="name?"
				style={styles.itemsend}
				onChangeText={(value) => this.controlName(value,index)}
				value={this.state.playerPool[index].name}
				/>


				{/*<Text style={styles.item}>{item.phoneNumber}</Text>*/}
				<TextInput
				keyboardType = {'phone-pad'}
				placeholder="Phone Number?"
				style={styles.itemsend}
				onChangeText={(value) => this.controlNumber(value,index)}
				value={this.state.playerPool[index].phoneNumber}
				/>

			</View>
		  )}
		/>

		{/*QuickLoad popup*/}
		<Modal
		backdropColor = {'gray'}
		backdropOpacity = {0.1}

		isVisible={this.state.quickAddModalVisible == true}>
			<View style={styles.modalContent}>
				<Text>Tap on the role you want to add another of to the game</Text>

				<FlatList
				data={this.state.rolePool}
				extraData={this.state}
				keyExtractor={(item) => item.name}
				renderItem={({item, index}) =>(

				<View>

					<TouchableOpacity
						style={styles.modalbuttonS}
						onPress={() => {
						this._quickAdd(index)
					  }}>
						<Text style={styles.modalbuttontext}>{item.name}</Text>
					</TouchableOpacity>


				</View>

				)}

				/>


			</View>

		</Modal>

		{/*quickRemove name delete*/}
		<Modal
		backdropColor = {'gray'}
		backdropOpacity = {0.1}

		isVisible={this.state.quickRemoveNameModalVisible == true}>
			<View style={styles.modalContent}>
				<Text>Tap on the name you want to delete from the game</Text>

				<FlatList
				data={this.state.playerPool}
				extraData={this.state}
				keyExtractor={(item) => item.name}
				renderItem={({item, index}) =>(

				<View>


					<TouchableOpacity
						style={styles.modalbuttonS}
						onPress={() => {
						this._quickRemoveName(index)
					  }}>
						<Text style={styles.modalbuttontext}>{item.name}</Text>
					</TouchableOpacity>

				</View>

				)}

				/>


			</View>

		</Modal>

		{/*quickRemove role delete*/}
		<Modal
		backdropColor = {'gray'}
		backdropOpacity = {0.1}

		isVisible={this.state.quickRemoveRoleModalVisible == true}>
			<View style={styles.modalContent}>
				<Text>Tap on the role you want to delete from the game</Text>

				<FlatList
				data={this.state.quickRemoveModifiedRoleList}
				extraData={this.state}
				keyExtractor={(item) => item.name}
				renderItem={({item, index}) =>(

				<View>

					<TouchableOpacity
						style={styles.modalbuttonS}
						onPress={() => {
						this._quickRemoveRole(item.name)
					  }}
					>
						<Text style={styles.modalbuttontext}>{item.name}</Text>
					</TouchableOpacity>

				</View>

				)}

				/>


			</View>

		</Modal>


		<Modal
			backdropColor = {'gray'}
			backdropOpacity = {0.1}
			isVisible={this.state.saveModal == 1}>
				<View style={styles.modalContent}>
					<Text style={{fontWeight:'bold'}}>Save</Text>

					<TextInput
					placeholder="Title"
					style={{width: '60%'}}
					onChangeText={(newtitle) => this.savetitle(newtitle)}
					value={this.state.savetitle}
					/>

					<Text>Players: {this.state.playerPool.length}</Text>



					<TouchableOpacity
						style={styles.modalbuttonS}
						onPress={() => {

						this.togglesaveModal(); this.togglesaveModal2();
					  }}>
						<Text style={styles.modalbuttontext}>OK</Text>
					</TouchableOpacity>

					<TouchableOpacity
						style={styles.modalbuttonS}
						onPress={() => {
						this.togglesaveModal();
						}}>
						<Text style={styles.modalbuttontext}>Cancel</Text>
					</TouchableOpacity>


				</View>
			</Modal>

			{/* Save */}
			<Modal
			backdropColor = {'gray'}
			backdropOpacity = {0.1}
			isVisible={this.state.saveModal2 == 1}>
				<View style={styles.modalContent}>
					<Text style={{fontWeight: 'bold'}}>Save</Text>


					<View style={{margin: '1%', flexDirection: 'row'}}>

						<TouchableOpacity
							style={styles.modalbuttonS}
							onPress={() => {
							this._storeData(1);this.togglesaveModal2();
						  }}>
							<Text style={styles.modalbuttontext}>{this.state.title1}</Text>
						</TouchableOpacity>


						<TouchableOpacity
							style={[styles.modalbuttonSdeleteGray, this.is1()]}
							disabled={this.state.title1=='Empty'}
							onPress={() => {
							this.deletetitle1()
						  }}>
							<Text style={styles.modalbuttontext}>X</Text>
						</TouchableOpacity>

					</View>

					<View style={{margin: '1%', flexDirection: 'row'}}>


						<TouchableOpacity
							style={styles.modalbuttonS}
							onPress={() => {
							this._storeData(2);this.togglesaveModal2();
						  }}>
							<Text style={styles.modalbuttontext}>{this.state.title2}</Text>
						</TouchableOpacity>



						<TouchableOpacity
							style={[styles.modalbuttonSdeleteGray, this.is2()]}
							disabled={this.state.title2=='Empty'}
							onPress={() => {
							this.deletetitle2()
						  }}>
							<Text style={styles.modalbuttontext}>X</Text>
						</TouchableOpacity>

					</View>

					<View style={{margin: '1%', flexDirection: 'row'}}>


						<TouchableOpacity
							style={styles.modalbuttonS}
							 onPress={() => {
							this._storeData(3); this.togglesaveModal2();
						  }}>
							<Text style={styles.modalbuttontext}>{this.state.title3}</Text>
						</TouchableOpacity>


						<TouchableOpacity
							style={[styles.modalbuttonSdeleteGray, this.is3()]}
							disabled={this.state.title3=='Empty'}
							onPress={() => {
							this.deletetitle3()
						  }}>
							<Text style={styles.modalbuttontext}>X</Text>
						</TouchableOpacity>

					</View>

					<View style={{margin: '1%', flexDirection: 'row'}}>

						<TouchableOpacity
							style={styles.modalbuttonS}
							onPress={() => {
							this.togglesaveModal2();
						  }}>
							<Text style={styles.modalbuttontext}>Cancel</Text>
						</TouchableOpacity>

					</View>

				</View>
			</Modal>


		{/* Load */}
		<Modal
			backdropColor = {'gray'}
			backdropOpacity = {0.1}
			isVisible={this.state.loadModal == 1}>
				<View style={styles.modalContent}>
					<Text style={{fontWeight: 'bold'}}>Load</Text>


					<View style={{margin: '1%', flexDirection: 'row'}}>

						<TouchableOpacity
							style={styles.modalbuttonS}
							onPress={() => {
							this._retrieveData(1);this.toggleloadModal();
						  }}>
							<Text style={styles.modalbuttontext}>{this.state.title1}</Text>
						</TouchableOpacity>


						<TouchableOpacity
							style={[styles.modalbuttonSdeleteGray, this.is1()]}
							disabled={this.state.title1=='Empty'}
							onPress={() => {
							this.deletetitle1()
						  }}>
							<Text style={styles.modalbuttontext}>X</Text>
						</TouchableOpacity>

					</View>

					<View style={{margin: '1%', flexDirection: 'row'}}>

						<TouchableOpacity
							style={styles.modalbuttonS}
							onPress={() => {
							this._retrieveData(2);this.toggleloadModal();
						  }}>
							<Text style={styles.modalbuttontext}>{this.state.title2}</Text>
						</TouchableOpacity>

						<TouchableOpacity
							style={[styles.modalbuttonSdeleteGray, this.is2()]}
							disabled={this.state.title2=='Empty'}
							onPress={() => {
							this.deletetitle2()
						  }}>
							<Text style={styles.modalbuttontext}>X</Text>
						</TouchableOpacity>

					</View>

					<View style={{margin: '1%', flexDirection: 'row'}}>

						<TouchableOpacity
							style={styles.modalbuttonS}
							onPress={() => {
							this._retrieveData(3); this.toggleloadModal();
						  }}>
							<Text style={styles.modalbuttontext}>{this.state.title3}</Text>
						</TouchableOpacity>


						<TouchableOpacity
							style={[styles.modalbuttonSdeleteGray, this.is3()]}
							disabled={this.state.title3=='Empty'}
							onPress={() => {
							this.deletetitle3()
						  }}>
							<Text style={styles.modalbuttontext}>X</Text>
						</TouchableOpacity>

					</View>

					<View style={{margin: '1%', flexDirection: 'row'}}>

						<TouchableOpacity
							style={styles.modalbuttonS}
							onPress={() => {
							this.toggleloadModal();
						  }}>
							<Text style={styles.modalbuttontext}>Cancel</Text>
						</TouchableOpacity>

					</View>

				</View>
			</Modal>


			<Modal
			backdropColor = {'gray'}
			backdropOpacity = {0.1}
			isVisible={this.state.guideModal == 1}>
				<View style={styles.modalContent}>
					<Text style={styles.helpfultitle}>Guide</Text>


					<Text style={styles.helpfultext}>Pass this device around and have everyone enter their numbers. Then hit the send button to send them all a text with their role information</Text>

					<CheckBox
					  title="Don't show again"
					  checked={this.state.checkedSendText}
					  onPress={() =>
						{
							this.setState({checkedSendText: !this.state.checkedSendText})
						}
					  }
					/>


					<TouchableOpacity
						style={styles.modalbuttonS}
						onPress={() => {
							this.setState({guideModal: false});
							this._storeDataSendText();
						}}
					>
						<Text style={styles.modalbuttontext}>OK</Text>
					</TouchableOpacity>


				</View>
			</Modal>


      </View>
    );
  }
}
