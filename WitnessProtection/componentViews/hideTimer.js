import React, {Component} from 'react';
import { View, Text, StyleSheet, ScrollView, Dimensions, Vibration, TouchableOpacity } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import {Button, Icon} from 'react-native-elements'
import Modal from 'react-native-modal'; // 2.4.0

//import ScrollPicker from 'react-native-wheel-scroll-picker';
import styles from '../styles';


widthD = Dimensions.get('window').width;
heightD = Dimensions.get('window').height;
globalFontsize = 17.82;
timermarginsize = 2;

export default class hideTimer extends React.Component {
  
  static navigationOptions = {
    title: 'Hide Timer',
  };
  
   constructor(props) {
		super(props);
		const { navigation } = this.props;
		
		const onlineT = navigation.getParam('online', 'NO-ID');
		const offlineT = navigation.getParam('offline', 'NO-ID');
		this.state = {sec: '00', 
			min: 4,
			tempmin: 4,
			tempsec: '00',
			selectedItem : 2,
			buttonstate: 'play',
			modalVisible: false,
			online: onlineT,
			offline: offlineT,
		} 
	}
  
  toggleModal() {
	console.log('original: '+this.state.modalVisible);
    newstate = this.state;
	newstate.modalVisible = (!(this.state.modalVisible));
	this.setState(newstate);
	console.log('nwe: '+this.state.modalVisible);
  }
  
  
  
  
  updateTimer(){
		newstate = this.state;
		if(newstate.sec == 0 && newstate.min > 0){
			newstate.sec = 59;
			newstate.min--;
		}
		else{
			newstate.sec--;
		}
		newstate.sec = ("0" +  newstate.sec).slice(-2);
		this.setState(newstate);
	}
	
  
 _play(){
	this.interval = setInterval(
		() => this.updateTimer(),
		1000
	); 
 }
  
  _pause(){
	clearInterval(this.interval);  
  }
  
  _stop(){
	this.setState({timer: 0}); 
	this.props.navigation.navigate('playTimer', {online: this.state.online, offline:this.state.offline});  
  }
  
  _edit(){
	this.toggleModal();  
  }
  
  _choose(){
	  if(this.state.buttonstate == 'play'){
		  this._play();
		  newstate = this.state;
		  newstate.buttonstate = 'pause';
		  this.setState(newstate);
	  }
	  else if(this.state.buttonstate == 'pause'){
		  this._pause();
		  newstate = this.state;
		  newstate.buttonstate = 'play';
		  this.setState(newstate);
	  }
	  /* else if(this.state.buttonstate == 'resume'){
		  this._play();
		  newstate = this.state;
		  newstate.buttonstate = 'pause';
		  this.setState(newstate);
	  } */
  }
  
	componentDidMount(){//will auto start the timer
	  /* this.interval = setInterval(
		() => this.updateTimer(),
		1000
	  ); */
	}
	
	

	componentDidUpdate(){
	  if(this.state.sec == 0 && this.state.min == 0){
		clearInterval(this.interval); 
		Vibration.vibrate([0,1000,1000,1000]);
	    this.props.navigation.navigate('playTimer');  
	  }
	}
	
	componentWillUnmount(){
		clearInterval(this.interval);
	}
	
	addmin(){
		newstate = this.state;
		newstate.tempmin=newstate.tempmin+1;
		this.setState(newstate);
	}
  
	submin(){
		newstate = this.state;
		newstate.tempmin=newstate.tempmin-1;
		this.setState(newstate);
	}
	
	addsec(){
		newstate = this.state;
		if(parseInt(newstate.tempsec)==59){
			newstate.tempsec='00';
			newstate.tempmin = newstate.tempmin+1;
		}
		else{
			newstate.tempsec=parseInt(newstate.tempsec)+1;
			newstate.tempsec = ("0" +  newstate.tempsec).slice(-2);
		}
		this.setState(newstate);
	}
	
	subsec(){
		newstate = this.state;
		if(parseInt(newstate.tempsec)==0){
			newstate.tempsec=59;
			newstate.tempmin = newstate.tempmin-1;
		}
		else{
			newstate.tempsec=parseInt(newstate.tempsec)-1;
			newstate.tempsec = ("0" +  newstate.tempsec).slice(-2);
		}
		this.setState(newstate);
	}
	
	ok(){
		console.log('temp min: '+this.state.tempmin);
		console.log('temp sec: '+this.state.tempsec);
		newstate = this.state;
		newstate.min = newstate.tempmin;
		newstate.sec = newstate.tempsec;
		this.setState(newstate);
		this.toggleModal();
	}
	cancel(){
		newstate = this.state;
		newstate.tempmin = 4;
		newstate.tempsec = '00';
		this.setState(newstate);
		this.toggleModal();
	}
 
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
		 
		 <View style = {styles.textheaderboxcolor}>
		<View style = {styles.textheaderbox}>
		 <Text style = {styles.littletext}> The Spy should reveal themselves and go hide. They have until this timer runs out to do so</Text>
		</View>
		</View>
		
		  
		  <View style={styles.headerboxtimercolor}>
			<View style={styles.headerboxtimer} containerViewStyle={{ width: '100%', height: '10%', marginbottom: '2%' }}> 
				<Text style = {styles.timertext}> {this.state.min} </Text>
				<Text style = {styles.timertext}> : </Text>
				<Text style = {styles.timertext}> {this.state.sec} </Text>
			</View>
			</View>
			
		   <View style={styles.timerheaderboxcolor} >
		  <View style={styles.timerheaderbox} >
				
				
				<Icon
				  name={this.state.buttonstate}
				  type='font-awesome'
				  iconStyle={styles.timericon}
				  onPress={() => {
					this._choose()
				  }} />
				
				
				<Icon
				  name='stop'
				  type='font-awesome'
				  iconStyle={styles.timericon}
				  onPress={() => {
					this._stop()
				  }} />
				
				<Icon
				  name='pencil'
				  type='font-awesome'
				  iconStyle={styles.timericon}
				  onPress={() => {
					this._edit()
				  }} />
				
				
			</View>
			</View>
			
			
		{/*Edit Time popup*/}
		<Modal 
		backdropColor = {'gray'}
		backdropOpacity = {0.1}
		 isVisible={this.state.modalVisible}>
			
			
			<View style={styles.modalContent}>
				
				<View style={styles.timerContainer} containerViewStyle={{ width: '100%', height: '20%', margin: '5%' }}>
					
						
						<TouchableOpacity
							style={styles.littlebutton}
							onPress={() => {
							this.addmin()
						  }}
							>
							<Text style={styles.littlebuttontext}>+</Text>
						</TouchableOpacity>
						
						
						<TouchableOpacity
							style={styles.littlebutton}
							onPress={() => {
							this.addsec()
						  }}>
							<Text style={styles.littlebuttontext}>+</Text>
						</TouchableOpacity>
					
				
				</View>
				
				
				<View style={styles.timerContainer} containerViewStyle={{ width: '100%', height: '20%', margin: '5%' }}>
				
						<Text style={styles.biggerlittlebuttontext}> {this.state.tempmin} </Text>
						<Text style={styles.biggerlittlebuttontext}> : </Text>
						<Text style={styles.biggerlittlebuttontext}> {this.state.tempsec} </Text>			
				
				</View >
				
				
				<View style={styles.timerContainer} containerViewStyle={{ width: '100%', height: '20%', margin: '5%' }}>
				
					
					<TouchableOpacity
							style={styles.littlebutton}
							onPress={() => {
						this.submin()
					  }}>
							<Text style={styles.littlebuttontext}>-</Text>
						</TouchableOpacity>
					
				
					
					<TouchableOpacity
							style={styles.littlebutton}
							onPress={() => {
						this.subsec()
					  }}>
							<Text style={styles.littlebuttontext}>-</Text>
						</TouchableOpacity>
				
				</View>
				
				<View style={{ width: '100%',
						height: '8%',
						marginTop: '14%',
						flexDirection: 'row',
						alignItems: 'center',
				justifyContent:'center',}} 
						
						containerViewStyle={{ 
						width: '100%', height: '20%', marginTop: '10%' 
						
						}}>
						
				
					
					
					<TouchableOpacity
							style={styles.OKCancelbuttons}
							onPress={() => {
						this.cancel()
					  }}>
							<Text style={styles.littlebuttontext}>Cancel</Text>
						</TouchableOpacity>
					
					
				
					
					<TouchableOpacity
							style={styles.OKCancelbuttons}
							onPress={() => {
						this.ok()
					  }}
							>
							<Text style={styles.littlebuttontext}>OK</Text>
						</TouchableOpacity>
					
				</View>
				
				
			</View>
		  
		</Modal>
		  
		
      </View>
    );
  }
}


