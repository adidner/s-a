import React from 'react';
import { AsyncStorage, View, Text, StyleSheet, FlatList, TextInput, Alert, TouchableHighlight, TouchableOpacity,PanResponder} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Modal from 'react-native-modal'; // 2.4.0
import { Button, CheckBox } from 'react-native-elements'
import Swiper from 'react-native-swiper';

import player from '../classes/classesPlayers';
import styles from '../styles';


export default class altsendingSMS extends React.Component {

  constructor(props) {
		super(props);
		const { navigation } = this.props;
		const rolePoolT = navigation.getParam('rolePoolParam', 'NO-ID');
		const totalRolesT = navigation.getParam('totalRolesParam', 'NO-ID');

		playerPoolT = [];
		for(i = 0;i<totalRolesT;i++){
			tempPlayer = new player("","");
			playerPoolT.push(tempPlayer);
		}


		console.log('in assign roles');
		oneToOneList = [];
		for(j = 0; j < rolePoolT.length; j++){
			for(i = 0; i<rolePoolT[j].roleNumber; i++){
				rolecopy = rolePoolT[j];
				oneToOneList.push(rolecopy);
			}
		}

		oneToOneList.sort(function(a, b){return 0.5 - Math.random()});
		for(i = 0; i< playerPoolT.length; i++){
			playerPoolT[i].playerRole = oneToOneList[i];
		}

		tempswipingList = [];
		i=0
		for(x=0;i<playerPoolT.length;i++){
			tempswipingList.push({key: playerPoolT[i].playerRole.name,name: playerPoolT[i].playerRole.name ,alignment: playerPoolT[i].playerRole.alignment, objective: playerPoolT[i].playerRole.objective,power: playerPoolT[i].playerRole.power});
			console.log('tempswipinglist name: '+tempswipingList[i].name);
		}
		i++;
		tempswipingList.push({key: 'last',name: 'last'});


		this.state = {
			rolePool: rolePoolT,
			totalRoles: totalRolesT,
			playerPool: playerPoolT,
			searchResult: null,
			quickAddModalVisible: false,
			quickRemoveNameModalVisible: false,
			quickRemoveRoleModalVisible: false,
			quickRemoveModifiedRoleList: [],
			swipingList:tempswipingList,
			viewFlex: 1,
			altFlex: 0,
			online: false,
			offline: true,
			guideModal: false,
			checkedOffline: false,
		}


		this._panResponder = PanResponder.create({
			  onStartShouldSetPanResponder: () => true,
			  onMoveShouldSetPanResponder: () => false,
			  onPanResponderGrant: (e, gestureState) => {
				this.onLongPressTimeout = setTimeout(() => {
				  console.log("ON LONG PRESS", gestureState);
				  this._changeState();
				}, 500);
			  },
			  onPanResponderRelease: () => {
				clearTimeout(this.onLongPressTimeout);
				this._changeBack();
			  },
			  onPanResponderTerminate: () => {
				clearTimeout(this.onLongPressTimeout);
				this._changeBack();
			  },
			  onShouldBlockNativeResponder: () => false,
			  onPanResponderTerminationRequest: () => true
			});

	}





  static navigationOptions = {
    title: 'Offline',
  };






	_seeRole(name, alignment,power,objective){
		Alert.alert(
			name,
			'Alignment: ' + alignment+"\n\n" +
			'Objective: ' + objective + "\n\n" +
			'Powers: ' + power + "\n\n"
		);
	}


		_changeState(){
			newstate = this.state;
			newstate.viewFlex = 0;
			newstate.altFlex = 1;
			this.setState(newstate);
		}

		_changeBack(){
			newstate = this.state;
			newstate.viewFlex = 1;
			newstate.altFlex = 0;
			this.setState(newstate);
		}


	_storeDataOffline = async () => {
	  try {

		string = '@checkedOffline:key';
		savedata='';
		if(!this.state.checkedOffline){
			savedata = 'true';
		}
		else{
			savedata='false';
		}
		await AsyncStorage.setItem(string, savedata);
	  } catch (error) {
		// Error saving data
		console.log('error in asyncStorage Roles')
	  }
	}

	_retrieveDataOffline = async () => {
		console.log('in retrieveData Roles');
	  try {
		string = '@checkedOffline:key';
		const value = await AsyncStorage.getItem(string);
		console.log("Retrieved Value: "+  value);
		if (value == null || value=='true') {
			newstate=this.state;
			newstate.guideModal=true;
			this.setState(newstate);
			return;
		}
		else{
			newstate=this.state;
			newstate.guideModal=false;
			this.setState(newstate);
			return;
		}
	   } catch (error) {
		 // Error retrieving data
		 console.log('error in retrieveData');
	   }
	}


	componentDidMount(){
	  this._retrieveDataOffline();
	}



  render() {

	let renderChildern = this.state.swipingList.map( (item,i)=>
	{
		if(item.name=='last'){
			return(
					<View style = {{width: '100%',height: '100%', margin: '4%', alignItems: 'center', justifyContent: 'center',}} key={item.name}>

						<TouchableOpacity
							style={styles.buttonS}
							onPress={() => this.props.navigation.navigate('hideTimer',{online: this.state.online,offline:this.state.offline})}
						>
							<Text style={styles.buttontext}>Lets Play!!!</Text>
						</TouchableOpacity>



					</View>
			);
		}
		else{
			return(
			<View style={styles.slide1}
				key={item.name}

				{...this._panResponder.panHandlers}

			>


				<View style = {{opacity: this.state.viewFlex}}>
					<Text style = {styles.textrole}>Touch and hold to see your role!</Text>

					<Text style = {styles.textrole}>Remember you're objective and power </Text>

					<Text style = {styles.textrole} >Once you're done swipe to the right and hand the phone to the someone who hasn't gotten a role yet</Text>
				</View>

				<View style = {{opacity: this.state.altFlex, padding: 7}}>
					<Text style = {styles.textrole}>name: { item.name } </Text>
					<Text style = {styles.textrole}>alignment: {item.alignment}</Text>
					<Text style = {styles.textrole}>objective: {item.objective}</Text>
					<Text style = {styles.textrole}>power: {item.power}</Text>
				</View>


			</View>
			);
		}
	});


    return (
		<View style={{flex: 1, backgroundColor: '#9DD6EB'}} contentContainerStyle = {{alignItems: 'center', justifyContent: 'center', margin: '2%'}}>

			<Modal
			backdropColor = {'gray'}
			backdropOpacity = {0.1}
			isVisible={this.state.guideModal == 1}>
				<View style={styles.modalContent}>
					<Text style={styles.helpfultitle}>Guide</Text>


					<Text style={styles.helpfultext}>Pass this phone around to everyone. Tap and hold to see your role, then swipe and pass so someone else can see theirs</Text>




					<CheckBox
					  title="Don't show again"
					  checked={this.state.checkedOffline}
					  onPress={() =>
						{
							this.setState({checkedOffline: !this.state.checkedOffline})
						}
					  }
					/>


					<TouchableOpacity
						style={styles.modalbuttonS}
						onPress={() => {
							this.setState({guideModal: false});
							this._storeDataOffline();
						}}
					>
						<Text style={styles.modalbuttontext}>OK</Text>
					</TouchableOpacity>


				</View>
			</Modal>

			<View style = {{width: '100%',height: '100%'}}>
				<Swiper
					 loop={false}
				>
					{renderChildern}

				</Swiper>
			</View>

		</View>


    );
  }
}
