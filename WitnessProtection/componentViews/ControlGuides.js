import React from 'react';
import { View, Text, StyleSheet, AsyncStorage, ScrollView, Dimensions, TouchableOpacity } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import {Button,CheckBox} from 'react-native-elements'

import styles from '../styles';




export default class FAQ extends React.Component {
  
  static navigationOptions = {
    title: 'Control Guides',
  };
  
  constructor(props) {
		super(props);
		const { navigation } = this.props;
		
		
		this.state = {
			checkedRoles: true,
			checkedOffline: true,
			checkedSendText: true,
		} 
	}
	
	
	_retrieveDataRoles = async () => {
		console.log('in retrieveData Roles');
	  try {
		string = '@checkedRoles:key';
		const value = await AsyncStorage.getItem(string);
		console.log("Retrieved Value: "+  value);
		if (value == null || value=='true') {
			return;
		}
		else{
			newstate=this.state;
			newstate.checkedRoles=false;
			this.setState(newstate);
			return;
		}		
	   } catch (error) {
		 // Error retrieving data
		 console.log('error in retrieveData');
	   }
	}
	
	_retrieveDataOffline = async () => {
		console.log('in retrieveData Offline');
	  try {
		string = '@checkedOffline:key';
		const value = await AsyncStorage.getItem(string);
		console.log("Retrieved Value: "+  value);
		if (value == null || value=='true') {
			return;
		}
		else{
			newstate=this.state;
			newstate.checkedOffline=false;
			this.setState(newstate);
			return;
		}		
	   } catch (error) {
		 // Error retrieving data
		 console.log('error in retrieveData');
	   }
	}
	
	_retrieveDataSendText = async () => {
		console.log('in retrieveData Send Text');
	  try {
		string = '@checkedSendText:key';
		const value = await AsyncStorage.getItem(string);
		console.log("Retrieved Value: "+  value);
		if (value == null || value=='true') {
			return;
		}
		else{
			newstate=this.state;
			newstate.checkedSendText=false;
			this.setState(newstate);
			return;
		}		
	   } catch (error) {
		 // Error retrieving data
		 console.log('error in retrieveData');
	   }
	}
	
	
	_storeDataRoles = async () => {
	  try {
		
		string = '@checkedRoles:key';
		savedata='';		
		if(!this.state.checkedRoles){
			savedata = 'true';
		}
		else{
			savedata='false';
		}
		await AsyncStorage.setItem(string, savedata);
		this.setState({checkedRoles: !this.state.checkedRoles});
	  } catch (error) {
		// Error saving data
		console.log('error in asyncStorage Roles')
	  }
	}
	
	_storeDataOffline = async () => {
	  try {
		string = '@checkedOffline:key';
		savedata='';
		if(!this.state.checkedOffline){
			savedata = 'true';
		}
		else{
			savedata='false';
		}
		await AsyncStorage.setItem(string, savedata);
		this.setState({checkedOffline: !this.state.checkedOffline});
	
	  } catch (error) {
		// Error saving data
		console.log('error in asyncStorage Offline')
	  }
	}
	
	_storeDataSendText = async () => {
	  try {
		
		string = '@checkedSendText:key'; 
		savedata='';		
		if(!this.state.checkedSendText){
			savedata = 'true';
		}
		else{
			savedata='false';
		}
		await AsyncStorage.setItem(string, savedata);
		this.setState({checkedSendText: !this.state.checkedSendText});
	  
	  } catch (error) {
		// Error saving data
		console.log('error in asyncStorage SendText')
	  }
	}
	
	
	
	
	
	
	
	componentDidMount(){
	  this._retrieveDataRoles();
	  this._retrieveDataOffline();
	  this._retrieveDataSendText();
	}
	
	
 
  
  render() {
    return (
      <View style={{ flex: 1, margin: '3%' }}>
        
		<CheckBox
		  title='Show Guide for Choose Roles Screen'
		  checked={this.state.checkedRoles}
		  onPress={() => 
			{
				
				this._storeDataRoles();
				console.log(this.state);
			}
		  
		  }
		/>
		
		<CheckBox
		  title='Show Guide for Play Offline Screen'
		  checked={this.state.checkedOffline}
		  onPress={() => 
			{
				this._storeDataOffline();
				console.log(this.state);
			}
		  
		  }
		/>
		
		<CheckBox
		  title='Show Guide for Send Text Screen'
		  checked={this.state.checkedSendText}
		  onPress={() => 
			{
				this._storeDataSendText();
				
			}
		  
		  }
		/>
		
		
		
		<TouchableOpacity
			style={styles.buttonS}
			onPress={() => this.props.navigation.navigate('Home')}
		>
			<Text style={styles.buttontext}>Done</Text>
		</TouchableOpacity>
      </View>
    );
  }
}


