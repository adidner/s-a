import React, { Component } from 'react';
import { View, Text, FlatList, TextInput, StyleSheet, Alert, Button, TouchableOpacity } from 'react-native';
//import { Button } from 'react-native-elements'; // Version can be specified in package.json
import { Permissions, Contacts } from 'expo';
import { createStackNavigator } from 'react-navigation';

import player from '../classes/classesPlayers';

import styles from '../styles';

export default class alternatecontactSearch extends Component {
     

	constructor(props) {
		super(props);
		const { navigation } = this.props;

		this.state = {contacts: [{key:'default nothign'}], contactlist: ""};

	}

	static navigationOptions = {
    title: 'Search Contacts',
  };

  async showFirstContactAsync(changedtext) {


	  console.log("in aysync");

	  // Ask for permission to query contacts.
      const permission = await Permissions.askAsync(Permissions.CONTACTS);

      if (permission.status !== 'granted') {
        // Permission was denied...
		console.log("denied");
        return;
      }
	  {/*800 to get your first 800 contacts*/}
      const contacts = await Contacts.getContactsAsync({
        fields: [
          Contacts.PHONE_NUMBERS,
          Contacts.EMAILS,
        ],
		pageSize: 800,
        pageOffset: 0,
      });



      if (contacts.total > 0) {
		newcontactlist = [];
		for(var i =0; i < contacts.data.length; i++){

			if(contacts.data[i].name.includes(changedtext)){
				newcontactlist.push({key: contacts.data[i].name, phoneNumber: contacts.data[i].phoneNumbers[0].number});
			}
		}

		this.setState({contactlist: newcontactlist});

      }
	  else{
		Alert.alert(
          'You have no contacts'
        );
	  }

    }

	doesthing(){
		console.log('in does things functino');
	}

	_backToSendingSMS(name,number){


		this.props.navigation.state.params.refresh(name,number);
		this.props.navigation.navigate('sendingSMS')
	}


  render() {
    return (



	  <View style={{padding: 10}}

	  keyboardShouldPersistTaps="always">
		<TextInput
          style={styles.textinput}
          placeholder="Enter a contacts name"
          onChangeText={(changedtext) => this.showFirstContactAsync(changedtext)}
        />
		<FlatList
		  keyboardShouldPersistTaps="always"
          data={this.state.contactlist}
		  extraData={this.state}
          renderItem={({item}) =>
				<TouchableOpacity
					style={styles.searchbutton}
					onPress={() => this._backToSendingSMS(item.key,item.phoneNumber)}
				>
					<Text style={styles.buttontext}>{item.key}</Text>
				</TouchableOpacity>


				}
			/>
		</View>

    );
  }
}
