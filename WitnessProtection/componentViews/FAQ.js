import React from 'react';
import { View, Text, StyleSheet, ScrollView, Dimensions } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import {Button} from 'react-native-elements'

import styles from '../styles';

widthD = Dimensions.get('window').width;
heightD = Dimensions.get('window').height;
globalFontsize = 17.82;


export default class FAQ extends React.Component {
  
  static navigationOptions = {
    title: 'FAQ',
  };
  
tab = "        ";
  
Q1 = "Can the Spy move?";
A1 = "Yes, yes they can";

Q2 = "Who dies when the Clumsy Ally is murdered?";
A2 = "Both people involved in the interaction die";

Q3 = "What happens if the identity thief doesn't have a role when the game ends?";  
A3 = "He/She looses the game";

Q4 = "Is my information being kept private?"; 
A4 = "See the privacy policy in settings";

Q5 = "How are these text messages sent?";
A5 = "Your phone uses Wifi or mobile data to send a signal to my servers hosted by Zeit. My servers process that request and use the Twilio API to send text messages. The number that sends text messages is owned by me through Twilio";

Q6 = "Why won't the sending text messages feature work?"; 
A6 = "This feature requires Wifi or mobile data so check that those features of your phone are working";	
  
  
  render() {
    return (
      <View style={{ flex: 1, margin: '3%' }}>
		<ScrollView>
		
			<View style={{marginVertical: '5%'}}>
				<Text style={styles.faqq}>{this.Q1}</Text>
				<Text style={styles.faqa}>{this.A1}</Text>
			</View>
			
			<View style={{marginVertical: '5%'}}>
				<Text style={styles.faqq}>{this.Q2}</Text>
				<Text style={styles.faqa}>{this.A2}</Text>
			</View>
			
			<View style={{marginVertical: '5%'}}>
				<Text style={styles.faqq}>{this.Q3}</Text>
				<Text style={styles.faqa}>{this.A3}</Text>
			</View>
			
			<View style={{marginVertical: '5%'}}>
				<Text style={styles.faqq}>{this.Q4}</Text>
				<Text style={styles.faqa}>{this.A4}</Text>
			</View>
			
			<View style={{marginVertical: '5%'}}>
				<Text style={styles.faqq}>{this.Q5}</Text>
				<Text style={styles.faqa}>{this.A5}</Text>
			</View>
			
			<View style={{marginVertical: '5%'}}>
				<Text style={styles.faqq}>{this.Q6}</Text>
				<Text style={styles.faqa}>{this.A6}</Text>
			</View>
		
		</ScrollView>
      </View>
    );
  }
}


