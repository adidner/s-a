import React from 'react';
import { View, Text, StyleSheet, ScrollView, Dimensions, TouchableHighlight, TouchableOpacity } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import {Button} from 'react-native-elements'
import Swiper from 'react-native-swiper';


import styles from '../styles';

widthD = Dimensions.get('window').width;
heightD = Dimensions.get('window').height;
globalFontsize = 17.82;





export default class howToPlay extends React.Component {
  
  static navigationOptions = {
    title: 'How To Play',
  };
  
  _method(){
	 
  }
  
  
  constructor(props) {
		super(props);
		const { navigation } = this.props;
		const rolePoolT = navigation.getParam('rolePoolParam', 'NO-ID');
		this.state = {rolePool: rolePoolT}
	}
  
  
  //var {height, width} = Dimensions.get('window');
 
  
  
  step1 = "Everyone will be given roles (no one should know who anyone else is so be careful)\n\n";
  step2 = "The witness will reveal himself and then go hide (a timer is set, this is how long the witness has to hide, usually about 4 minutes)\n\n";
  step3 = "After the hide timer runs out, everyone will be allowed to leave and try to achieve their objective (explained in your role text) also a game timer will start, usually about 12 minutes.\n\n";
  step4 = "Good guys are trying to find and hide with the witness if ⅓ of all players find and hide with the witness in this manner good guys win. \n\n";
  step5 = "Bad guys are trying to find and kill the witness or have the game timer run out. \n\n";
  step6 = "There are also third party roles with objectives separate from the struggle between good and bad. \n\n";
  step7 = "When you die, sit down in place and be quiet, you may not talk to living players unless they have a power which explicitly allows you to talk to them. \n\n";
  step8 = "Generally it is a good idea to explain all the roles and their powers to new players to keep them from being confused and intimate their first few rounds. \n\n";
  
  
  
  text1 = "Establish boundaries for the game, which players should stay inside at all times. Boundaries could be rooms in a house or the edges of a park.";
  text2 = "Explain all the roles being used in the game to the group. For example, “The Detective can talk to dead players, the Murderer can kill by running their finger across your throat” ";
  text3 = "Everyone will be given roles, either via text messages from the app or by passing the phone around to play in offline mode. Players should keep the role they are assigned secret.";
  text4 = "The player with the role of Spy announces themselves to the group and gets four minutes to hide. If there is a Mole or Escort in the game, they go with the Spy.";
  text6 = "After four minutes, a twelve minute game timer is set, and the game starts! Players begin to move around and try to achieve their objective.";
  text7 = "Good guys are trying to find and hide with the Spy. If a number of good players equal to ⅓ of the total number of players find and hide with the Spy, the Good team wins. ";
  text8 = "Bad guys are trying to find and kill the Spy or have the game timer run out. If the Spy is killed or the game timer runs out without enough Good players hiding with the Spy, the Bad team wins.";
  text9 = "Death: when a player dies, they have to stop moving and sit down. They cannot speak to other players while dead unless a role specifically allows them to do so. (EX, the detective)";
  text10 = "The easiest way to understand the game is to play it! If players seem confused, try playing a practice round so that everyone gets a chance to try the game for themselves.";
  
  
  render() {
    return (
      
			<Swiper loop={false}>
				<View style={styles.slide1}>
				   <Text style={styles.howtoplaytext}>{this.text1}</Text>
				</View>
				<View style={styles.slide2}>
					<Text style={styles.howtoplaytext}>{this.text2}</Text>
				</View>
				<View style={styles.slide3}>
				  <Text style={styles.howtoplaytext}>{this.text3}</Text>
				</View>
				<View style={styles.slide1}>
				  <Text style={styles.howtoplaytext}>{this.text4}</Text>
				</View>
				<View style={styles.slide2}>
				  <Text style={styles.howtoplaytext}>{this.text6}</Text>
				  <Text style={styles.howtoplaytext}>{this.text7}</Text>
				  <Text style={styles.howtoplaytext}>{this.text8}</Text>
				</View>
				<View style={styles.slide3}>
				  <Text style={styles.howtoplaytext}>{this.text9}</Text>
				</View>
				<View style={styles.slide1}>
				  <Text style={styles.howtoplaytext}>{this.text10}</Text>
				</View>
				<View style={styles.slide2}>
										
					<View>
						<TouchableOpacity
							style={styles.buttonS}
							onPress={() => this.props.navigation.navigate('RolesandAbilities',{rolePoolParam: this.state.rolePool})}
						>
						<Text style={styles.buttontext}>Roles and Abilities</Text>
						</TouchableOpacity>
						
						
						<TouchableOpacity
							style={styles.buttonS}
							onPress={() => this.props.navigation.navigate('Home')}
						>
						<Text style={styles.buttontext}>Done</Text>
						</TouchableOpacity>
					</View>
					
					
				</View>
			</Swiper>
		 
    );
  }
}



