import React from 'react';
import { AsyncStorage, TextInput, View, Text, FlatList, StyleSheet, TouchableOpacity, TouchableHighlight, Alert} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import {CheckBox, Icon} from 'react-native-elements'
import {Button} from 'react-native-elements'
import Modal from 'react-native-modal'; // 2.4.0

import styles from '../styles';

export default class DetailsScreen extends React.Component {

  constructor(props) {
		super(props);
		const { navigation } = this.props;
		rolePoolT = navigation.getParam('rolePoolParam', 'NO-ID');

		rolePoolT[1].roleNumber=1; //index is 1 because the first rolePool thing is the title GOOD ROLES
								   //God I suck at design, next time these shouldn't be the same class type probably

		this.state = {rolePool: rolePoolT,
		totalRoles: 1,
		visibleModalsave1: null,
		checked: true,
		date:null,
		slot1:'empty1D',
		slot2:'empty2D',
		slot3:'empty3D',
		title1: 'in1',
		title2: 'in2',
		title3: 'in3',
		saveModal: false,
		saveModal2: false,
		loadModal: false,
		savetitle: '',
		guideModal: false,
		checkedRoles: false,
		}
		console.log(this.state);
	}

	togglesaveModal(){
	   newState = this.state;
	   newState.saveModal = !(this.state.saveModal);
	   this.setState(newState);
	   console.log('in save modal toggle');
	}
	togglesaveModal2(){
	   newState = this.state;
	   newState.saveModal2 = !(this.state.saveModal2);
	   this.setState(newState);
	}

	toggleloadModal(){
	   newState = this.state;
	   newState.loadModal = !(this.state.loadModal);
	   this.setState(newState);

	}
	componentDidMount(){

		/* newstate = this.state;
		var date = new Date().getDate();
		var month = new Date().getMonth() + 1;
		var year = new Date().getFullYear();

		var completedate = (date + '-' + month + '-' + year);
		newstate.date = completedate; */


		//newstate.slot1 = this._retrieveDataCheck('save1');
		//newstate.slot2 = this._retrieveDataCheck('save2');
		//newstate.slot3 = this._retrieveDataCheck('save3');

		//newstate.slot1 = 'test';

		/*for(i = 0;i<newstate.rolePool.length;i++){
			newstate.totalRoles = newstate.rolePool[i].roleNumber +  newstate.totalRoles;
		}

		this.setState(newstate);*/
	}

  static navigationOptions = {
    title: 'Choose Roles',

  };

  toggleModal(visible) {
      this.setState({ modalVisiblesave1: visible });
   }

	_storeData = async (slot) => {
	  console.log('in _storeData')
	  try {
		string = '@SaveR'+slot+':state';
		savestate = JSON.stringify(this.state);
		await AsyncStorage.setItem(string, savestate);
	  } catch (error) {
		// Error saving data
		console.log('error in asyncStorage')
	  }
	}

	_retrieveData = async (slot) => {
		console.log('in retrieveData');
	  try {
		string = '@SaveR'+slot+':state';
		const value = await AsyncStorage.getItem(string);
		console.log("Retrieved Value: "+  value);
		if (value != null) {
			console.log("title value: "+ value.savetitle);
			objectversion = JSON.parse(value);
			objectversion.saveModal2=false;
			this.setState(objectversion);
			return;
		}
		else{
			console.log('null section');
			return;
		}
	   } catch (error) {
		 // Error retrieving data
		 console.log('error in retrieveData');
	   }
	}

  savetitle(newtitle){
	 newstate = this.state;
	 newstate.savetitle=newtitle;
	 this.setState(newstate);
  }

	deletetitle1 = async () => {
	  try {
		this.setState({title1:'Empty'})
		string = '@SaveR1:state';
		await AsyncStorage.removeItem(string);
	  } catch (error) {
		// Error retrieving data
		console.log(error.message);
	  }
	}

	deletetitle2 = async () => {
	  try {
		this.setState({title2:'Empty'})
		string = '@SaveR2:state';
		await AsyncStorage.removeItem(string);
	  } catch (error) {
		// Error retrieving data
		console.log(error.message);
	  }
	}

	deletetitle3 = async () => {
	  try {
		this.setState({title3:'Empty'})
		string = '@SaveR3:state';
		await AsyncStorage.removeItem(string);
	  } catch (error) {
		// Error retrieving data
		console.log(error.message);
	  }
	}

  settitles = async () => {
	  console.log('in newgettitlessss');
	  try {
		string = '@SaveR1:state';
		const value = await AsyncStorage.getItem(string);
		if (value != null) {
			objectversion = JSON.parse(value);
			console.log("title object, save: " + string + ",  Title:" + objectversion.savetitle);
			this.setState({title1:objectversion.savetitle})
		}
		else{
			console.log('in the null section');
			this.setState({title1:'Empty'});
		}
	   } catch (error) {
		 // Error retrieving data
		 console.log('error in retrieveData');
	   }

	   try {
		string = '@SaveR2:state';
		const value = await AsyncStorage.getItem(string);
		if (value != null) {
			objectversion = JSON.parse(value);
			console.log("title object, save: " + string + ",  Title:" + objectversion.savetitle);
			this.setState({title2:objectversion.savetitle})
		}
		else{
			console.log('in the null section');
			this.setState({title2:'Empty'});
		}
	   } catch (error) {
		 // Error retrieving data
		 console.log('error in retrieveData');
	   }

	   try {
		string = '@SaveR3:state';
		const value = await AsyncStorage.getItem(string);
		if (value != null) {
			objectversion = JSON.parse(value);
			console.log("title object, save: " + string + ",  Title:" + objectversion.savetitle);
			this.setState({title3:objectversion.savetitle})
		}
		else{
			console.log('in the null section');
			this.setState({title3:'Empty'});
		}
	   } catch (error) {
		 // Error retrieving data
		 console.log('error in retrieveData');
	   }
	}






	//fixing background coloring things,

	renderItem = ({item, index}) => {

			//console.log(item.alignment);
			if(item.name=='Spy'){
				return(
				<View style={styles.colorgood}>
				<View style={styles.rolechoosinggood}>

					<TouchableOpacity
						style={styles.buttonrolerow}
						onPress={() => {
						Alert.alert(
						item.name,
						'Alignment: ' + item.alignment+"\n\n" +
						'Objective: ' + item.objective + "\n\n" +
						'Powers: ' + item.power + "\n\n"
						);
					  }}
					>
					<Text style={styles.modalbuttontext}>?</Text>
					</TouchableOpacity>


					<Text style={styles.flastListWords}>{item.name}</Text>

					<Text style={styles.flatListNumber}>{item.roleNumber}</Text>

					<TouchableOpacity
						style={styles.buttonrolerowfaded}
						disabled={true}
						onPress={() => {

						{/*newlist = this.state.rolePool;
						newtotalRoles = this.state.totalRoles;

						if(newlist[index].roleNumber > 0){
							newtotalRoles = newtotalRoles-1;
							newlist[index].roleNumber = newlist[index].roleNumber-1;
						}
						this.setState({rolePool: newlist, totalRoles: newtotalRoles});*/}
						}}
					>
					<Text style={styles.modalbuttontext}>-</Text>
					</TouchableOpacity>




					<TouchableOpacity
						style={styles.buttonrolerowfaded}
						disabled={true}
						onPress={() => {
							{/*newlist = this.state.rolePool;
						  newtotalRoles = this.state.totalRoles;
						  newtotalRoles = newtotalRoles+1;
						  newlist[index].roleNumber = newlist[index].roleNumber+1;
							this.setState({rolePool: newlist, totalRoles: newtotalRoles});*/}
					  }}
					>
					<Text style={styles.modalbuttontext}>+</Text>
					</TouchableOpacity>



				</View>
				</View>
				)
			}

			if(item.alignment=='Good'){
			return(
				<View style={styles.colorgood}>
				<View style={styles.rolechoosinggood}>

					<TouchableOpacity
						style={styles.buttonrolerow}
						onPress={() => {
						Alert.alert(
						item.name,
						'Alignment: ' + item.alignment+"\n\n" +
						'Objective: ' + item.objective + "\n\n" +
						'Powers: ' + item.power + "\n\n"
						);
					  }}
					>
					<Text style={styles.modalbuttontext}>?</Text>
					</TouchableOpacity>


					<Text style={styles.flastListWords}>{item.name}</Text>

					<Text style={styles.flatListNumber}>{item.roleNumber}</Text>

					<TouchableOpacity
						style={styles.buttonrolerow}
						onPress={() => {

						newlist = this.state.rolePool;
						newtotalRoles = this.state.totalRoles;

						if(newlist[index].roleNumber > 0){
							newtotalRoles = newtotalRoles-1;
							newlist[index].roleNumber = newlist[index].roleNumber-1;
						}
						this.setState({rolePool: newlist, totalRoles: newtotalRoles});
						}}
					>
					<Text style={styles.modalbuttontext}>-</Text>
					</TouchableOpacity>




					<TouchableOpacity
						style={styles.buttonrolerow}
						onPress={() => {
						  newlist = this.state.rolePool;
						  newtotalRoles = this.state.totalRoles;
						  newtotalRoles = newtotalRoles+1;
						  newlist[index].roleNumber = newlist[index].roleNumber+1;
						  this.setState({rolePool: newlist, totalRoles: newtotalRoles});
					  }}
					>
					<Text style={styles.modalbuttontext}>+</Text>
					</TouchableOpacity>



				</View>
				</View>
				)
			}
			else if(item.alignment=='Bad'){
			return(
			<View style={styles.colorbad}>
			<View style={styles.rolechoosingbad}>

					<TouchableOpacity
						style={styles.buttonrolerow}
						onPress={() => {
						Alert.alert(
						item.name,
						'Alignment: ' + item.alignment+"\n\n" +
						'Objective: ' + item.objective + "\n\n" +
						'Powers: ' + item.power + "\n\n"
						);
					  }}
					>
					<Text style={styles.modalbuttontext}>?</Text>
					</TouchableOpacity>


					<Text style={styles.flastListWords}>{item.name}</Text>

					<Text style={styles.flatListNumber}>{item.roleNumber}</Text>

					<TouchableOpacity
						style={styles.buttonrolerow}
						onPress={() => {

						newlist = this.state.rolePool;
						newtotalRoles = this.state.totalRoles;

						if(newlist[index].roleNumber > 0){
							newtotalRoles = newtotalRoles-1;
							newlist[index].roleNumber = newlist[index].roleNumber-1;
						}
						this.setState({rolePool: newlist, totalRoles: newtotalRoles});
						}}
					>
					<Text style={styles.modalbuttontext}>-</Text>
					</TouchableOpacity>




					<TouchableOpacity
						style={styles.buttonrolerow}
						onPress={() => {
						  newlist = this.state.rolePool;
						  newtotalRoles = this.state.totalRoles;
						  newtotalRoles = newtotalRoles+1;
						  newlist[index].roleNumber = newlist[index].roleNumber+1;
						  this.setState({rolePool: newlist, totalRoles: newtotalRoles});
					  }}
					>
					<Text style={styles.modalbuttontext}>+</Text>
					</TouchableOpacity>


				</View>
				</View>
				)
			}
			else if(item.alignment=='GoodT'){
			return(
				<View style={styles.colorbadtitle}>
				<View style={styles.rolechoosinggoodtitle}>
					<Text style={styles.roletitles}>{item.name}</Text>
				</View>
				</View>
				)
			}
			else if(item.alignment=='BadT'){
			return(
				<View style={styles.colorgoodtitle}>
				<View style={styles.rolechoosingbadtitle}>
					<Text style={styles.roletitles}>{item.name}</Text>
				</View>
				</View>
				)
			}

	}


	is1(){
		if(this.state.title1=='Empty'){
			return '';
		}
		else{
			return styles.modalbuttonSjustred;
		}
	}
	is2(){
		if(this.state.title2=='Empty'){
			return '';
		}
		else{
			return styles.modalbuttonSjustred;
		}
	}
	is3(){
		if(this.state.title3=='Empty'){
			return '';
		}
		else{
			return styles.modalbuttonSjustred;
		}
	}

	_storeDataRoles = async () => {
	  try {

		string = '@checkedRoles:key';
		savedata='';
		if(!this.state.checkedRoles){
			savedata = 'true';
		}
		else{
			savedata='false';
		}
		await AsyncStorage.setItem(string, savedata);
	  } catch (error) {
		// Error saving data
		console.log('error in asyncStorage Roles')
	  }
	}

	_retrieveDataRoles = async () => {
		console.log('in retrieveData Roles');
	  try {
		string = '@checkedRoles:key';
		const value = await AsyncStorage.getItem(string);
		console.log("Retrieved Value: "+  value);
		if (value == null || value=='true') {
			newstate=this.state;
			newstate.guideModal=true;
			this.setState(newstate);
			return;
		}
		else{
			newstate=this.state;
			newstate.guideModal=false;
			this.setState(newstate);
			return;
		}
	   } catch (error) {
		 // Error retrieving data
		 console.log('error in retrieveData');
	   }
	}


	componentDidMount(){
	  this._retrieveDataRoles();
	}




  render() {

    return (


		<View style={{ width: '100%', height:'100%', alignItems: 'center', justifyContent: 'center' }} >

			<View style={styles.headerbox} containerViewStyle={{ width: '100%', height: '10%'}}>


				<View style= {styles.iconcontainer}>
					<Icon
					 onPress={() => {
						this.settitles();this.toggleloadModal();
					  }}
					  name='md-download'
					  type='ionicon'
					  iconStyle={styles.roleheadericon}
					/>
					<Text style={styles.iconbelowtext}> Load </Text>
				</View>

				<View style= {styles.iconcontainer}>
					<Icon
					  name='save'
					  type='foundation'
					  onPress={() => {
						 this.settitles();this.togglesaveModal();
					  }}
					  iconStyle={styles.roleheadericon}
					/>
					<Text style={styles.iconbelowtext}> Save </Text>
				</View>

				<View style= {styles.iconcontainer}>
					<Icon
					 onPress={() => {this.props.navigation.navigate('sendingSMS',
					{rolePoolParam:this.state.rolePool, totalRolesParam:this.state.totalRoles})}}

					  name='message'
					  type='material-community-icons'
					  iconStyle={styles.roleheadericon}
					/>
					<Text style={styles.iconbelowtext}> Text </Text>
				</View>

				<View style= {styles.iconcontainer}>
					<Icon
					 onPress={() => this.props.navigation.navigate('altsendingSMS',
				{rolePoolParam:this.state.rolePool, totalRolesParam:this.state.totalRoles})}
					  name='wifi-off'
					  type='material-community'
					  iconStyle={styles.roleheadericon}
					/>
					<Text style={styles.iconbelowtext}> Offline </Text>
				</View>


			</View>


			<FlatList
			  data={this.state.rolePool}
			  extraData={this.state}
			  keyExtractor={(item) => item.name}
			  renderItem= {this.renderItem}

			/>

			<View>
				<Text style={styles.totalbar}> Total: {this.state.totalRoles} </Text>
			</View>


      {/*First save*/}
			<Modal
			backdropColor = {'gray'}
			backdropOpacity = {0.1}
			isVisible={this.state.saveModal == 1}>
				<View style={styles.modalContent}>
					<Text style={{fontWeight: 'bold'}}>Save</Text>

					<TextInput
					placeholder="Title"
					style={{width: '60%'}}
					onChangeText={(newtitle) => this.savetitle(newtitle)}
					value={this.state.savetitle}
					/>


					<TouchableOpacity
						style={styles.modalbuttonS}
						onPress={() => {

						this.togglesaveModal(); this.togglesaveModal2();
					  }}
					>
						<Text style={styles.modalbuttontext}>OK</Text>
					</TouchableOpacity>


          <TouchableOpacity
            style={styles.modalbuttonS}
            onPress={() => {
              this.togglesaveModal();
              }}
          >
            <Text style={styles.modalbuttontext}>Cancel</Text>
          </TouchableOpacity>


				</View>
			</Modal>

			{/* Save */}
			<Modal
			backdropColor = {'gray'}
			backdropOpacity = {0.1}
			isVisible={this.state.saveModal2 == 1}>
				<View style={styles.modalContent}>
					<Text style={{fontWeight: 'bold'}}>Save</Text>


					<View style={{margin: '1%', flexDirection: 'row'}}>

						<TouchableOpacity
							style={styles.modalbuttonS}
							onPress={() => {
							this._storeData(1);this.togglesaveModal2();
						  }}
						>
							<Text style={styles.modalbuttontext}>{this.state.title1}</Text>
						</TouchableOpacity>


						<TouchableOpacity
							style={[styles.modalbuttonSdeleteGray, this.is1()]}
							disabled={this.state.title1=='Empty'}
							onPress={() => {
							this.deletetitle1()
						  }}
						>
							<Text style={styles.modalbuttontext}>X</Text>
						</TouchableOpacity>

					</View>

					<View style={{margin: '1%', flexDirection: 'row'}}>


						<TouchableOpacity
							style={styles.modalbuttonS}
							onPress={() => {
								this._storeData(2);this.togglesaveModal2();
							  }}
							>
							<Text style={styles.modalbuttontext}>{this.state.title2}</Text>
						</TouchableOpacity>


						<TouchableOpacity
							style={[styles.modalbuttonSdeleteGray, this.is2()]}
							disabled={this.state.title2=='Empty'}
							onPress={() => {
								this.deletetitle2()
							  }}
						>
							<Text style={styles.modalbuttontext}>X</Text>
						</TouchableOpacity>


					</View>

					<View style={{margin: '1%', flexDirection: 'row'}}>




						<TouchableOpacity
							style={styles.modalbuttonS}
							onPress={() => {
							this._storeData(3); this.togglesaveModal2();
						  }}
						>
							<Text style={styles.modalbuttontext}>{this.state.title3}</Text>
						</TouchableOpacity>


						<TouchableOpacity
							style={[styles.modalbuttonSdeleteGray, this.is3()]}
							disabled={this.state.title3=='Empty'}
							onPress={() => {
							this.deletetitle3()
						  }}
						>
							<Text style={styles.modalbuttontext}>X</Text>
						</TouchableOpacity>

					</View>

					<View style={{margin: '1%', flexDirection: 'row'}}>


						<TouchableOpacity
							style={styles.modalbuttonS}
							onPress={() => {
								this.togglesaveModal2();
							  }}
						>
							<Text style={styles.modalbuttontext}>Cancel</Text>
						</TouchableOpacity>

					</View>

				</View>
			</Modal>


		{/* Load */}
		<Modal
			backdropColor = {'gray'}
			backdropOpacity = {0.1}
			isVisible={this.state.loadModal == 1}>
				<View style={styles.modalContent}>
					<Text style={{fontWeight: 'bold'}}>Load</Text>


					<View style={{margin: '1%', flexDirection: 'row'}}>

						<TouchableOpacity
							style={styles.modalbuttonS}
							onPress={() => {
								this._retrieveData(1);this.toggleloadModal();
							  }}
						>
							<Text style={styles.modalbuttontext}>{this.state.title1}</Text>
						</TouchableOpacity>



						<TouchableOpacity
							style={[styles.modalbuttonSdeleteGray, this.is1()]}
							disabled={this.state.title1=='Empty'}
							onPress={() => {
								this.deletetitle1()
							  }}
						>
							<Text style={styles.modalbuttontext}>X</Text>
						</TouchableOpacity>

					</View>

					<View style={{margin: '1%', flexDirection: 'row'}}>


						<TouchableOpacity
							style={styles.modalbuttonS}
							onPress={() => {
								this._retrieveData(2);this.toggleloadModal();
							  }}
						>
							<Text style={styles.modalbuttontext}>{this.state.title2}</Text>
						</TouchableOpacity>



						<TouchableOpacity
							style={[styles.modalbuttonSdeleteGray, this.is2()]}
							disabled={this.state.title2=='Empty'}
							 onPress={() => {
								this.deletetitle2()
							  }}
						>
							<Text style={styles.modalbuttontext}>X</Text>
						</TouchableOpacity>
					</View>

					<View style={{margin: '1%', flexDirection: 'row'}}>


						<TouchableOpacity
							style={styles.modalbuttonS}
							onPress={() => {
								this._retrieveData(3); this.toggleloadModal();
							  }}
						>
							<Text style={styles.modalbuttontext}>{this.state.title3}</Text>
						</TouchableOpacity>



						<TouchableOpacity
							style={[styles.modalbuttonSdeleteGray, this.is3()]}
							disabled={this.state.title3=='Empty'}
							onPress={() => {
							this.deletetitle3()
						  }}
						>
							<Text style={styles.modalbuttontext}>X</Text>
						</TouchableOpacity>

					</View>

					<View style={{margin: '1%', flexDirection: 'row'}}>

						<TouchableOpacity
							style={styles.modalbuttonS}
							onPress={() => {
								this.toggleloadModal();
							}}
						>
							<Text style={styles.modalbuttontext}>Cancel</Text>
						</TouchableOpacity>

					</View>

				</View>
			</Modal>





			<Modal
			backdropColor = {'gray'}
			backdropOpacity = {0.1}
			isVisible={this.state.guideModal == 1}>
				<View style={styles.modalContent}>
					<Text style={styles.helpfultitle}>Guide</Text>


					<Text style={styles.helpfultext}>Choose as many roles as you have people. About 1/3 bad and 2/3 good.</Text>




					<CheckBox
					  title="Don't show again"
					  checked={this.state.checkedRoles}
					  onPress={() =>
						{
							this.setState({checkedRoles: !this.state.checkedRoles})
						}
					  }
					/>


					<TouchableOpacity
						style={styles.modalbuttonS}
						onPress={() => {
							this.setState({guideModal: false});
							this._storeDataRoles();
						}}
					>
						<Text style={styles.modalbuttontext}>OK</Text>
					</TouchableOpacity>


				</View>
			</Modal>


		</View>


    );
  }
}
