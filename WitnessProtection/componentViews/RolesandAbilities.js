import React, { Component } from 'react';
import { View, Text, FlatList, TextInput, StyleSheet, Alert } from 'react-native';
import { Button } from 'react-native-elements'; // Version can be specified in package.json
import { Permissions, Contacts } from 'expo';
import { createStackNavigator } from 'react-navigation';

import styles from '../styles';
import Role from '../classes/classesRoles';



export default class RolesandAbilities extends Component {
  
  
	
  
	constructor(props) {
		super(props);
		const { navigation } = this.props;		
		const rolePoolT = navigation.getParam('rolePoolParam', 'NO-ID');
		this.state = {rolePool: rolePoolT} 
	}
	
	static navigationOptions = {
		title: 'Role and Abilities',
	
	};
	

	
	
	
	renderItem = ({item, index}) => {
		console.log()
		if (item.alignment == 'Good'){ 
			return( 
				<View>
					
					<Button
					  buttonStyle={styles.colorgoodRA}
					  onPress={() => {
						Alert.alert(
						item.name,
						'Alignment: ' + item.alignment+"\n\n" +
						'Objective: ' + item.objective + "\n\n" +
						'Powers: ' + item.power + "\n\n"
						);
					  }}
					  title={item.name}
					/>
					
				</View>
			)
		}
		else if (item.alignment == 'Bad'){ 
			return (
				<View >
					
					<Button
					  buttonStyle={styles.colorbadRA}
					  onPress={() => {
						Alert.alert(
						item.name,
						'Alignment: ' + item.alignment+"\n\n" +
						'Objective: ' + item.objective + "\n\n" +
						'Powers: ' + item.power + "\n\n"
						);
					  }}
					  title={item.name}
					/>
					
				</View>
			)
		}
		else if (item.alignment == 'BadT'){ 
			return (
				<View >
					
					<Button
					  buttonStyle={styles.colorbadtitleRA}
					  onPress={() => {
						Alert.alert(
						item.name,
						'The rolls below are bad, your game should generally have 1/3 of players be bad'
						);
					  }}
					  title={item.name}
					/>
					
				</View>
			)
		}
		else if (item.alignment == 'GoodT'){ 
			return (
				<View >
					
					<Button
					  buttonStyle={styles.colorgoodtitleRA}
					  onPress={() => {
						Alert.alert(
						item.name,
						'The rolls below are Good, your game should generally have 2/3 of players be Good'
						);
					  }}
					  title={item.name}
					/>
					
				</View>
			)
		}
		
	}
	
	
  render() {
    return (
	
      
	  
	<View style={{padding: 10}}>
		<FlatList
			  data={this.state.rolePool}
			  extraData={this.state}
			  keyExtractor={(item) => item.name}
			  renderItem={this.renderItem}
			  
			/>
			
	</View>
	
    );
  }
}