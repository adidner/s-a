
{/*react imports*/}
import React from 'react';
import { View, StyleSheet, Alert, Linking, TouchableOpacity, TouchableHighlight } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import {Button,Text, Icon} from 'react-native-elements';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';


{/*classes imports*/}
import Role from './classes/classesRoles';
import player from './classes/classesPlayers';


{/*componentView imports*/}
import howToPlay from './componentViews/howToPlay';
import alternatecontactSearch from './componentViews/alternatecontactSearch';
import roleChoosing from './componentViews/roleChoosing';
import sendingSMS from './componentViews/sendingSMS';
import altsendingSMS from './componentViews/altsendingSMS';
import hideTimer from './componentViews/hideTimer';
import playTimer from './componentViews/playTimer';
import RolesandAbilities from './componentViews/RolesandAbilities';
import FAQ from './componentViews/FAQ'
import PrivacyPolicy from './componentViews/PrivacyPolicy';
import TermsandConditions from './componentViews/TermsandConditions';
import ControlGuides from './componentViews/ControlGuides';


import styles from './styles';


import { MenuProvider } from 'react-native-popup-menu';


import SvgUri from 'react-native-svg-uri';

import SvgComponent from './Img/title.js';

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';

/*
https://www.smooth-code.com/open-source/svgr/playground/
Used to convert SVG code into a react component which can be exported and used however
*/



goodtitle = new Role("GOOD ROLES", "GoodT", "(Hide Well)");

spy = new Role("Spy", "Good", "(Hide Well)");
spy.objective = "Get a number of good-aligned players equal to ⅓ of the total number of players to hide with you before time runs out or you are killed by the evil players.";

clumsyAlly = new Role("Clumsy Ally", "Good", "The first person you touch (accidental or intentional) dies");
resistanceFighter = new Role("Resistance Fighter", "Good", "Once during the game, you can kill another player by running your hand across their neck.");
SWAT = new Role("SWAT","Good","If you see a player die, you may kill the player who caused the death");
police = new Role("Police","Good","You can freeze other player for 15 seconds by grabbing their wrists, you do not need to stay with the player while they are frozen");
doctor = new Role("Doctor", "Good", " You can revive dead people by drawing a cross on their arm");
interrogator = new Role("Interrogator","Good","If you place your hand on another player’s shoulder, they must tell you their role.");
detective = new Role("Detective","Good","You can speak with dead players");
escort = new Role("Escort", "Good", "Reveals themselves at the beginning of the game and helps the witness hide.");
protectedDignitary = new Role("Protected Dignitary", "Good", "Immune to all negative effects except death. This player’s killer also dies.");


badtitle = new Role("BAD ROLES", "BadT", "(Hide Well)");

mole = new Role("Mole", "Bad", "Reveal yourself as an escort at the beginning of the game and help the witness hide.");
suicideBomber = new Role("Suicide Bomber","Bad","Can hug someone and yell “BOOM!” and all players in a 5 ft. radius, including the Bomber, are killed.");
blackMailer = new Role("Black Mailer", "Bad", "As long as you are touching the Spy, the good guys can’t win.");
assassin = new Role("Assassin", "Bad", "You can kill players by running your hand across their neck");
mercenary = new Role("Mercenary", "Bad", "You can kill players by running your hand across their neck. After you kill place your hand on their shoulder to force them to reveal their role to you. If they were Bad you can no longer kill");
kidnapper = new Role("Kidnapper", "Bad", "Grab another player’s elbow to keep them from moving away from you, using their Power, or speaking.");
traitor = new Role("Traitor", "Bad", "If a player asks you what your role is they die. If a player tells you their role they also die");
identityThief = new Role("Identity Thief","Bad", "Useable once: shake hands with a dead player to copy their role. You cannot speak with dead players. ");


var rolePool = [];
rolePool.push(goodtitle);
rolePool.push(spy);
rolePool.push(clumsyAlly);
rolePool.push(resistanceFighter);
rolePool.push(SWAT);
rolePool.push(police);
rolePool.push(doctor);
rolePool.push(interrogator);
rolePool.push(detective);
rolePool.push(escort);
rolePool.push(protectedDignitary);

rolePool.push(badtitle);
rolePool.push(mole);
rolePool.push(suicideBomber);
rolePool.push(blackMailer);
rolePool.push(assassin);
rolePool.push(mercenary);
rolePool.push(kidnapper);
rolePool.push(traitor);
//rolePool.push(identityThief);

class HomeScreen extends React.Component {
  static navigationOptions = ({navigation})=>({
    title: 'Main Menu',

  });

  constructor() {
    super();
    this.state = {
      isOpen: false,
      isDisabled: false,
      swipeToClose: true,
      sliderValue: 0.3
    };
  }


  render() {


	{/*important that these go before the return but inside the render*/}
	{/*name, alignment (Good or Evil), power*/}



    return (

      <View style={styles.container}>


		<View

		style = {{backgroundColor: 'white' , width: '100%', height: verticalScale(405), alignItems: 'center', justifyContent: 'center',}}
		>
		{/*<Text style={styles.titlesecrets}>Secrets</Text>
		<Text style={styles.titleand}>&</Text>
		<Text style={styles.titleassasins}>Assassins</Text>*/}

		{/*<View>
			<SvgUri
			  width="350"
			  height="350"
			  source={require('./Img/AppTitleIcon.svg')}
			/>
		</View>*/}

		<SvgComponent height={verticalScale(350)} width={scale(350)}>

		</SvgComponent>


		</View>


		{/*important to have the braces around the class use*/}
		{/*<Text>{rolePool[1].details}</Text>*/}

		<View
		style = {{backgroundColor: 'gray' , width: '100%', height: verticalScale(205), alignItems: 'center', justifyContent: 'center',}}>


		<TouchableOpacity
			style={styles.buttonS}
			onPress={() => this.props.navigation.navigate('howToPlay', {rolePoolParam: rolePool})}
		>
			<Text style={styles.buttontext}>How to Play</Text>
		</TouchableOpacity>

		<TouchableOpacity
			style={styles.buttonS}
			onPress={() => this.props.navigation.navigate('roleChoosing', {rolePoolParam:rolePool})}
		>
			<Text style={styles.buttontext}>Start!</Text>
		</TouchableOpacity>


		</View>

      </View>
    );
  }
}

function addtostring(currentstring, newtext){
	return currentstring+newtext;
}

currentstring = '';
currentstring = addtostring(currentstring, "Software Architect\t\t\t\t Aaron Didner\n");
currentstring = addtostring(currentstring, "Full Stack Developer\t\t Aaron Didner\n");
currentstring = addtostring(currentstring, "UI/UX Developer\t\t\t\t\t\t Ian Abbene\n");
currentstring = addtostring(currentstring, "Graphical Artist\t\t\t\t\t\t\t Ian Abbene\n");
currentstring = addtostring(currentstring, "Legal Advisor\t\t\t\t\t\t\t\t Joey Didner\n");
currentstring = addtostring(currentstring, "Beta Testers\n ");
currentstring = addtostring(currentstring, "\t\t Jeffery Da\n");
currentstring = addtostring(currentstring, "\t\t Gavin Cai\n");
currentstring = addtostring(currentstring, "\t\t Austin Martin\n");
currentstring = addtostring(currentstring, "\t\t Dominik Ferone\n");
currentstring = addtostring(currentstring, "\t\t Natasha Bolshakoff\n");




const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
	howToPlay: howToPlay,
	alternatecontactSearch: alternatecontactSearch,
	roleChoosing: roleChoosing,
	sendingSMS: sendingSMS,
	altsendingSMS: altsendingSMS,
	hideTimer: hideTimer,
	playTimer: playTimer,
	RolesandAbilities: RolesandAbilities,
	FAQ: FAQ,
	TermsandConditions: TermsandConditions,
	PrivacyPolicy: PrivacyPolicy,
	ControlGuides: ControlGuides,
  },
  {
    initialRouteName: 'Home',
	navigationOptions: ({ navigation }) => ({
		headerTitleStyle: styles.headertitle,
		headerStyle: styles.headerheight,
		headerRight: (
			<View>

				<Menu>
				  <MenuTrigger>
					<Icon
					type='AntDesign'
					name='settings'
					iconStyle={{fontSize: scale(20), margin: moderateScale(8)}}
					/>
				  </MenuTrigger>
				  <MenuOptions>
					<MenuOption onSelect={() => navigation.navigate('RolesandAbilities',{rolePoolParam:rolePool})} text='Roles and Abilities' customStyles={{optionText: styles.settingstext}} />
					<MenuOption onSelect={() => navigation.navigate('FAQ')} text='FAQ' customStyles={{optionText: styles.settingstext}}/>
					<MenuOption onSelect={()=>{ Linking.openURL('https://termsandconditions-ovvafpxwaf.now.sh')}} text='Terms of Use' customStyles={{optionText: styles.settingstext}}/>
					<MenuOption onSelect={()=>{ Linking.openURL('https://privacypolicy-qpfzpknfph.now.sh')}} text='Privacy Policy' customStyles={{optionText: styles.settingstext}}/>
					{/* A JSX comment <MenuOption onSelect={() => Alert.alert('Credits', currentstring)} text='Credits' customStyles={{optionText: styles.settingstext}}/> */}
					<MenuOption onSelect={() => {Linking.openURL('https://credits-xzezxdrybt.now.sh')}} text='Credits' customStyles={{optionText: styles.settingstext}}/>
					<MenuOption onSelect={() => {navigation.navigate('ControlGuides')}} text='Control Guide Popups' customStyles={{optionText: styles.settingstext}}/>
				  </MenuOptions>
				</Menu>
			</View>
		),



    }),
  }
);

const triggerStyles = {
  triggerText: {
    color: 'black',
	fontSize: scale(20),
  },
  triggerOuterWrapper: {
    marginRight:moderateScale(8),
  },
  triggerWrapper: {

  },
  triggerTouchableComponent: {

  },
};


export default class App extends React.Component {
  render() {
    return <MenuProvider><RootStack/></MenuProvider>;
  }
}
