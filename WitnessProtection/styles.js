import { StyleSheet } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

bad = 'rgb(123, 36, 28)';
good = 'rgb(46, 134, 193)';
badT = 'red';
goodT = 'lightblue';
buttoncolor = '#DDDDDD'; //background
globalFontsize = 17.82;
buttontextcolor = 'black';//should be black revert after testing
grayedoutbuttoncolor = '#B9B3B3';


export default StyleSheet.create({
  //for title screen
  //and header bar
  //and some gloabl general stuff
  titlesecrets:{
	color: good,
	fontSize: scale(46),
	fontWeight: 'bold',
	margin: moderateScale(5)
  },
  titleand:{
	color: 'gray',
	fontSize: scale(26),
	fontWeight: 'bold',
	margin: moderateScale(5)
  },
  titleassasins:{
	color: bad,
	fontSize: scale(46),
	fontWeight: 'bold',
	margin: moderateScale(5)
  },
  buttontext:{
	fontSize: scale(14),
	color: buttontextcolor,
  },
  buttonS:{
   width: scale(175),
   height: verticalScale(50),
   margin: moderateScale(5),
   alignItems: 'center',
   justifyContent: 'center',
   backgroundColor: buttoncolor,
  },
  settingstext:{
	fontSize: scale(14),
  },

  headerheight:{
	  height: verticalScale(60),
  },
  headertitle:{
	  fontWeight: 'bold',
	  fontSize: scale(20),
  },
  container: {
   alignItems: 'center',
   justifyContent: 'center',
   backgroundColor: 'pink',
   flex: 1,
  },
  roletitles:{
	  fontSize: scale(25),
	  fontWeight: 'bold',
	  margin: moderateScale(9),
  },

  //for how to play
  howtoplaytext:{
	  fontSize: scale(21),
	  color: buttontextcolor,
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
	padding: moderateScale(12),
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5',
	padding: moderateScale(12),
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
	padding: moderateScale(12),
  },




  //for role choosing
  buttonrolerow:{
   width: scale(35),
   height: verticalScale(50),
   margin: moderateScale(4),
   alignItems: 'center',
   justifyContent: 'center',
   backgroundColor: buttoncolor,
  },
  buttonrolerowfaded:{
   width: scale(35),
   height: verticalScale(50),
   margin: moderateScale(4),
   alignItems: 'center',
   justifyContent: 'center',
   backgroundColor: grayedoutbuttoncolor,
  },
  flatList:{
	alignItems: 'stretch'
  },
  flastListWords:{
	fontSize: scale(14),
	width: scale(110),
	height: verticalScale(50),
	textAlignVertical: 'center',
	color: 'white',
	margin: moderateScale(5),
  },
  flatListNumber:{
	fontSize: scale(14),
	width: scale(30),
	height: verticalScale(50),
	textAlignVertical: 'center',
	color: 'white',
	margin: moderateScale(5),
  },
  rolesandAbilitiesGood:{
	color: "blue",
  },
  rolesandAbilitiesBad:{
	color: "red",
  },
  colorbad:{
	alignItems: 'center',
	backgroundColor: bad,
  },
  colorgood:{
	alignItems: 'center',
	backgroundColor: good,

  },
  colorbadtitle:{
	 backgroundColor: badT,
  },
  colorgoodtitle:{
	  backgroundColor: goodT,

  },

  colorbadRA:{
	alignItems: 'center',
	backgroundColor: bad,
    borderWidth: moderateScale(1),
    borderColor: 'gray'
  },
  colorgoodRA:{
	alignItems: 'center',
	backgroundColor: good,
    borderWidth: moderateScale(1),
    borderColor: 'gray'
  },
  colorbadtitleRA:{
	 backgroundColor: badT,
	 borderWidth: moderateScale(1),
    borderColor: 'gray'
  },
  colorgoodtitleRA:{
	  backgroundColor: goodT,
	borderWidth: moderateScale(1),
    borderColor: 'gray'
  },


  rolechoosinggoodtitle:{
	flex: 1,
	alignItems: 'center',
	justifyContent: 'center',
     backgroundColor: goodT,

  },
  rolechoosingbadtitle:{
	flex: 1,
	alignItems: 'center',
	justifyContent: 'center',
     backgroundColor: badT,

  },
  rolechoosinggood:{
	flexDirection: 'row',
	flex:1,
	margin: moderateScale(7),
	backgroundColor: good,
  },
  rolechoosingbad:{
	flexDirection: 'row',
	flex:1,
	margin: moderateScale(7),
	backgroundColor: bad,
  },


  roleheadericon:{
	  fontSize: scale(35),
	  marginLeft: moderateScale(22),
	  marginRight: moderateScale(22),
	  marginTop: moderateScale(14),
	  marginBottom: moderateScale(1),
  },
  iconbelowtext:{
	  fontSize: scale(14),
  },
  iconcontainer:{
	alignItems: 'center',
	justifyContent: 'center',
  },


  totalbar:{
	  margin: scale(5),
	  fontSize: scale(16),
  },


  modalbuttontext:{
	fontSize: scale(14),
	color: buttontextcolor,
  },
  modalbuttonS:{
   width: scale(180),
   height: verticalScale(50),
   margin: moderateScale(3),
   alignItems: 'center',
   justifyContent: 'center',
   backgroundColor: buttoncolor,
  },
  modalbuttonSdeleteRed:{
   width: scale(60),
   height: verticalScale(50),
   margin: moderateScale(3),
   alignItems: 'center',
   justifyContent: 'center',
   backgroundColor: 'red',
  },
  modalbuttonSjustred:{
	  backgroundColor: 'red'
  },

  modalbuttonSdeleteGray:{
   width: scale(60),
   height: verticalScale(50),
   margin: moderateScale(3),
   alignItems: 'center',
   justifyContent: 'center',
   backgroundColor: grayedoutbuttoncolor,
  },



  //sendingSMS
  roleheadericonSMS:{
	  fontSize: scale(35),
	  marginLeft: moderateScale(13),
	  marginRight: moderateScale(13),
	  marginTop: moderateScale(14),
	  marginBottom: moderateScale(1),
  },
  iconbelowtextSMS:{
	  fontSize: scale(12),
  },
  iconcontainerSMS:{
	alignItems: 'center',
	justifyContent: 'center',
  },

  //Offline (AltsendingSMS)
  textrole:{
	fontSize: scale(18),
	padding: moderateScale(12),
  },


  //FAQ
  faqq:{
	fontWeight: 'bold',
	fontSize: scale(14),
  },
  faqa:{
	 marginHorizontal: '3%',
	fontSize: scale(14),
  },

  //alternatecontactSearch
  searchbutton:{
	width: scale(340),
    height: verticalScale(55),
    margin: moderateScale(2),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'silver',
  },
  textinput:{
	height: scale(50),
  },



  //timers
  timertext:{
	  fontSize: scale(70),
  },
  timericon:{
	fontSize: scale(50),
	margin: moderateScale(25),
  },
  timerheaderbox:{

	flexDirection: 'row',
	alignItems: 'center',
	justifyContent: 'center',
  },
  textheaderbox:{

	alignItems: 'center',
	justifyContent: 'center',
  },
  littletext:{
	fontSize: scale(20),
	margin: moderateScale(7),
  },
  headerboxtimer:{
	margin: moderateScale(7),
	flexDirection: 'row',
	alignItems: 'center',
	justifyContent: 'center',
	marginBottom: moderateScale(80),
	marginTop: moderateScale(50),

  },
  headerboxtimercolor:{
	backgroundColor: 'white',
	width: '100%',
  },
  textheaderboxcolor:{
	backgroundColor: 'red',
	width: '100%',
  },
  timerheaderboxcolor:{
	backgroundColor: 'green',
	width: '100%',
  },

  //timer modal
  littlebutton:{
	width: scale(40),
    height: verticalScale(55),
    margin: moderateScale(9),
    alignItems: 'center',
    justifyContent: 'center',
	backgroundColor: buttoncolor,
  },
  littlebuttontext:{
	  fontSize: scale(14),
	  margin: scale(10)
  },
  biggerlittlebuttontext:{
	fontSize: scale(25),
  },
  OKCancelbuttons:{
	width: scale(70),
    height: verticalScale(50),
    margin: moderateScale(9),
    alignItems: 'center',
    justifyContent: 'center',
	backgroundColor: buttoncolor,
  },

  //popup guides
  helpfultitle:{
    fontSize: scale(19),
    fontWeight: 'bold',
  },

  helpfultext:{
    fontSize: scale(15),
    margin: moderateScale(7),
  },






  //legacy
  headerbox:{
	margin: moderateScale(7),
	flex: 0.15,
	flexDirection: 'row',
	alignItems: 'center',
	justifyContent: 'center',
  },
  modal3: {
    height: 300,
    width: 300
  },
  btn: {
    margin: 10,
    backgroundColor: "#3B5998",
    color: "white",
    padding: 10
  },

  btnModal: {
    position: "absolute",
    top: 0,
    right: 0,
    width: 50,
    height: 50,
    backgroundColor: "transparent"
  },

  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  row: {
	flexDirection: 'row',
	flex:1,
	margin: 7,
  },



  button: {
    backgroundColor: 'lightblue',
    padding: 12,
    margin: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },

  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modal: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: '#f7021a',
	  borderRadius: 4,
      padding: 22
   },
   baseText: {
	fontSize: globalFontsize,
  },
  itemsend: {
    padding: 10,
    fontSize: 18,
    height: 44,
	width: 155,
  },

  wrapper: {
  },

  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },


  timerContainer:{
	  width: '100%',
	  height: '8%',
	  margin: '4%',
	  flexDirection: 'row',
	   alignItems: 'center',
	   justifyContent:'center',
  },

  timecontainerstyles:{
	marginLeft: 0,
	marginRight: 0,
  },


});
