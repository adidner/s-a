import React from 'react'
import Svg, { Defs, Text, Path } from 'react-native-svg'
/* SVGR has dropped some elements not supported by react-native-svg: style, title */

const SvgComponent = props => (
  <Svg
    id="prefix__Layer_1"
    data-name="Layer 1"
    viewBox="0 0 575 316.75"
    {...props}
  >
    <Defs />
    <Text
      transform="matrix(.7 0 0 .7 65.51 251.58)"
      fontFamily="POGODisplay,POGO"
      fill="#622"
      fontSize={129.66}
    >
      {'ASSASSINS'}
    </Text>
    <Text
      transform="matrix(.7 0 0 .7 115.46 124.16)"
      fill="#365d96"
      fontFamily="POGODisplay,POGO"
      fontSize={129.66}
    >
      {'SECRETS'}
    </Text>
    <Text
      transform="translate(263.87 169.27)"
      fill="#606060"
      fontSize={30.91}
      fontFamily="POGODisplay,POGO"
    >
      {'AND'}
    </Text>
    <Path
      className="prefix__cls-4"
      d="M242.91 155.33l-19.37-6.55-.6 13.15 19.97-6.6z"
    />
    <Path
      className="prefix__cls-4"
      d="M115.25 148.9l108.29-.16-.6 13.23-108.3.16.61-13.23z"
    />
    <Path
      className="prefix__cls-4"
      d="M115.46 144.42l-8.79.02-1.02 22.17 8.79-.01 1.02-22.18z"
    />
    <Path
      className="prefix__cls-4"
      d="M111.64 151.04l-21.79.03-.41 8.95 21.79-.03.41-8.95z"
    />
    <Path
      className="prefix__cls-4"
      d="M83.83 161.62l9.33-.01 4.94-6.08-4.38-6.05-9.33.01-4.95 6.07 4.39 6.06zM336.71 155.33l18.96-6.55.59 13.15-19.55-6.6z"
    />
    <Path
      className="prefix__cls-4"
      d="M461.63 148.9l-105.97-.16.6 13.23 105.97.16-.6-13.23z"
    />
    <Path
      className="prefix__cls-4"
      d="M461.43 144.42l8.6.02 1 22.17-8.6-.01-1-22.18z"
    />
    <Path
      className="prefix__cls-4"
      d="M465.17 151.04l21.32.03.4 8.95-21.32-.03-.4-8.95z"
    />
    <Path
      className="prefix__cls-4"
      d="M492.38 161.62l-9.13-.01-4.84-6.08 4.29-6.05 9.13.01 4.84 6.07-4.29 6.06z"
    />
  </Svg>
)

export default SvgComponent
