import React from 'react';

export default class Role {
  
  
  constructor(name,alignment,power) {
    this.name = name;
    this.alignment = alignment;
	if(alignment=='Good'){
		this.objective = "Find and hide with the Spy, if 1/3rd of all players do this then Good guys win";
	}
	else if(alignment=='Bad'){
		this.objective = "Find and kill the Spy, or delay the Good guys from getting 1/3rd of players to hide with the Spy until time runs out";
	}
	this.power = power;
	this.roleNumber = 0;
  }
}